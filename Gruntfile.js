module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'assets/scss',
          src: ['*.scss'],
          dest: 'public/assets/css',
          ext: '.css'
        }],
        options: {
          banner: '/* FORUM THEMER (c) 2014\n * Jamie Watson \n * http://www.jamiewatson.me\n */'
        }
      }
    },
    uglify: {
      all: {
        files: {
          'public/assets/js/app.min.js': [
            'assets/js/slick.min.js',
            'assets/js/app.js'
          ],
          'public/assets/js/billing.min.js': [
            'assets/js/billing.js'
          ]
        }
      }
    },
    watch: {
      options:{
        livereload: 1337,
      },
      js: {
        files: ['assets/js/*.js'],
        tasks: ['uglify']
      },
      css: {
        files: ['assets/scss/**/*.scss'],
        tasks: ['sass:dist']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-sass');

  grunt.registerTask('default', ['sass:dist','uglify','watch']);

};