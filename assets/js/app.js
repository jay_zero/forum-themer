$(function(){

	$('.project-images').slick({
		arrows:true,
		dots: false,
		autoplay: true,
		prevArrow: '.slider-prev',
		nextArrow: '.slider-next'
	});

	$('.copy').on('click', function(){
		window.location = '/cart/copyright/' + $(this).val();
	});

	$('.accordion-tabs-minimal').each(function(index) {
	    $(this).children('li').first().children('a').addClass('is-active').next().addClass('is-open').show();
	  });

	  $('.accordion-tabs-minimal').on('click', 'li > a', function(event) {
	    if (!$(this).hasClass('is-active')) {
	      event.preventDefault();
	      var accordionTabs = $(this).closest('.accordion-tabs-minimal')
	      accordionTabs.find('.is-open').removeClass('is-open').hide();

	      $(this).next().toggleClass('is-open').toggle();
	      accordionTabs.find('.is-active').removeClass('is-active');
	      $(this).addClass('is-active');
	    } else {
	      event.preventDefault();
	    }
	  });


	  //Move to forum.js
	  $('.like').on('click', function(e){
	  	e.preventDefault();
	  	var url = $(this).attr('href');
	  	var likes = $(this).parent().prevAll('.likes');
	  	var _this = $(this);
	  	$.ajax({url: url, dataType: 'json', success:function(data){
	  		_this.text( data.verb );
	  		likes.text( data.likes );
	  	}})
	  });


	  // pollEvents();

});


function pollEvents(){

	$.ajax({

        url: '/notifications',
        dataType: 'json',
        cache: false

    }).done( function(data) {
    	if( data.messages ){
	      $.each(data.messages, function(i, msg) { 
	      	toastr.info(msg.message);
	      });
	    }
      
   });

   setTimeout( 'pollEvents()', 5000 );
}