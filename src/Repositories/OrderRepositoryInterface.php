<?php 

namespace Themer\Repositories;

interface OrderRepositoryInterface{

	public function findAll();

	public function paginateByUserId( $user_id );	

	public function findByToken( $token );

	public function saveOrder( $uid, $token, $item );

}