<?php 

namespace Themer\Repositories;

interface OrderLineRepositoryInterface{

	public function findAllByOrderId( $order_id );

	public function createOrderLine( $order_id, $item );

}