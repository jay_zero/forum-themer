<?php 

namespace Themer\Repositories\Eloquent;

use Themer\Repositories\DownloadRepositoryInterface;

use Str;

class DownloadRepository extends BaseRepository implements DownloadRepositoryInterface{
	
	protected $model;

	public function __construct( \Themer\Models\Download $d){
		$this->model = $d;
	}

	public function generateNewToken( $user_id, $project_id ){

		$this->model->create( ['user_id'=>$user_id, 'project_id' => $project_id, 'token' => Str::random(32)])->save();

	}

	public function findByUserAndToken( $user_id, $project_id, $token ){
		return $this->model->with('project')->whereUserId($user_id)->whereToken($token)->firstOrFail();
	}

	public function findByUserAndProjectId( $user_id, $project_id ){
		return $this->model->whereUserId($user_id)->whereProjectId($project_id)->firstOrFail();
	}


	public function paginateByUserId( $uid ){
		return $this->model->with('project.software')->whereUserId( $uid )->paginate( \Config::get('app.paginate') );
	}
	// public function findFreeDownload( $project_id ){
	// 	return $this->model->whereHas('project', function($query){ 
	// 		$query->where('price', '=', '0')
	// 	})->whereProjectId($project_id)->firstOrFail();
	// }

}