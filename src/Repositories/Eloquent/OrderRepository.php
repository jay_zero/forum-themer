<?php 

namespace Themer\Repositories\Eloquent;

use Themer\Repositories\OrderRepositoryInterface;

use DB;
use Themer\Models\OrderLine;

class OrderRepository extends BaseRepository implements OrderRepositoryInterface{
	
	protected $model;

	protected $lines;

	public function __construct( \Themer\Models\Order $tag ){ //, OrderLinesRepositoryInterface $l ){
		$this->model = $tag;
		// $this->lines = $l;
	}

    public function getLatest(){
        return $this->model->with('user')->take(10)->get();
    }

	public function paginateAll(){
		return $this->model->paginate( \Config::get('site.paginate'));
	}

	public function paginateByUserId( $user_id ){
		return $this->model->whereUserId( $user_id )->paginate( \Config::get('site.paginate') );
	}	

	public function findByToken( $token ){

	}

    public function findById( $id, $uid ){
        return $this->model->with('lines.download','project.software')->whereUserId( $uid )->findOrFail($id);
    }

    public function findAll(){

    }

    public function saveOrder( $uid, $token, $subtotal ){

    	//This will need to be adjusted for cart implementation ($item to $items)
    	return $this->model->create( [ 'user_id' => $uid, 'stripeToken' => $token, 'total' => $subtotal ]);

    }

    public function getStats(){
    	return (object)[
    		'total_orders' 		=> $this->getTotalOrders(),
    		'average_spend'		=> $this->getAverageSpend(),
    		'items_per_basket'	=> $this->getAverageItemsPerBasket()
    	];
    }

    public function getTotalOrders(){
    	//Language file
    	return sprintf( "Across %s order(s) customers have spent &pound;%s.", number_format($this->model->count(), 0), number_format($this->model->order_total, 2) );
    }

    public function getAverageSpend(){
    	$avg = $this->model->avg('total')/100;
    	return sprintf( "Customers spend on average &pound;%s per order.", number_format($avg, 2));
    }

    public function getAverageItemsPerBasket(){
    	// Uh. Yeah.
    	//Not sure how to do this with eloquent o_O
    	$average = DB::select('SELECT AVG(totals) as total FROM ( SELECT COUNT(order_id) as totals FROM order_lines GROUP BY order_id ) r')[0]->total;
    	return sprintf( "On average, customers purchase %s items(s) per order.", number_format($average, 1));
    }


}