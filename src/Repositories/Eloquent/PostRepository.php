<?php 

namespace Themer\Repositories\Eloquent;

use Themer\Repositories\PostRepositoryInterface;

class PostRepository extends BaseRepository implements PostRepositoryInterface{
	
	protected $model;

	public function __construct( \Themer\Models\Post $post ){
		$this->model = $post;
	}

	public function findById( $id ){
		$post = $this->model->with('user','tags')->whereParent(0)->findOrFail( $id );
		$post->user->reputation = \Themer\Models\User::rep( $post->user->id );
		return $post;
	}

	public function findPostById( $id ){
		return $this->model->findOrFail( $id );
	}

	public function paginateReplies( $parent ){
		$posts = $this->model->with('user')->whereParent( $parent )->orderBy('created_at', 'asc')->paginate(15);

		//we'll change this at some point.
		foreach( $posts as $post )
			$post->user->reputation = \Themer\Models\User::rep( $post->user->id );

		return $posts;
	}

	public function paginateTopics( $slug = '' ){
		if( $slug !== '' ){

			return $this->model->with('user', 'replies')->whereHas('tags', function($query)use($slug){
				$query->whereSlug( $slug );
			})->orderBy('last_post', 'desc')->whereParent(0)->paginate(15);


		}else{
			return $this->model->with('user', 'replies')->orderBy('last_post', 'desc')->whereParent(0)->paginate(15);//\Config::get('app.paginate')
		}
	}

	public function create( $data ){
		$post = $this->model->newInstance();
		$post->title = isset($data['title']) ? $data['title'] : '';
		$post->user_id = $data['user_id'];
		$post->body = e($data['body']);
		$post->last_post = e($data['last_post']); //Can be removed at some point
		$post->parent = isset($data['parent']) ? $data['parent'] : 0;

		$post->save();

		if( isset($data['tags']) )
			$post->tags()->sync( $data['tags'] );

		$post->user()->increment('post_count');

		return $post;
	}

	public function savePost( $post, $data ){
		$post->body = e($data['body']);
		$post->save();

		return $post;
	}

	public function closeThreadById( $id ){
		$post = $this->findById( $id );

		//Change closed to timestamp
		$post->closed = 1;
		$post->timestamps = false;
		$post->last_post = date('Y-m-d H:i:s');
		$post->save();

		return $post;
	}

	public function deleteThreadById( $id ){
		$post = $this->findById( $id );
		$post->delete();
	}

	public function getStats(){
    	return (object)[
    		'total_threads' 	=> $this->getTotalThreads(),
    		'total_replies'		=> $this->getTotalReplies()
    	];
    }

    public function getTotalThreads(){
    	return $this->model->whereParent(0)->remember(1)->count();
    }

    public function getTotalReplies(){
    	return $this->model->where('parent', '>', 0)->remember(1)->count();
    }

}