<?php

namespace Themer\Repositories\Eloquent;

use Themer\Repositories\OrderLineRepositoryInterface;
use Config;

class OrderLineRepository extends BaseRepository implements OrderLineRepositoryInterface{

	protected $model;

	public function __construct( \Themer\Models\OrderLine $line ){
		$this->model = $line;
	}
	
	public function findAllByOrderId( $order_id ){
		$this->model->whereOrderId( $order_id )->get();
	}

	public function createOrderLine( $order_id, $item ){
		//Fields need to adjust with cart implementation currently expects a Project to be passed
		$this->model->create( [ 'order_id' => $order_id, 'item_name' => $item->name, 'item_cost' => $item->price, 'project_id' => $item->id, 'copyright' => $item->options->has('copyright')?1:0, 'copyright_cost' => Config::get('app.copyright_cost') ] );
	}

}