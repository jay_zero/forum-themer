<?php 

namespace Themer\Repositories\Eloquent;

use Themer\Repositories\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface{

	protected $model;

	public function __construct( \Themer\Models\User $user){
		$this->model = $user;
	}

	public function getMemberCount(){
		return $this->model->count();
	}

	//Should really place this in the base repo or something.  It's a common thing across the majority
	//of the repositories.
	public function paginateAll(){
		return $this->model->paginate( \Config::get('site.paginate'));
	}


}