<?php 

namespace Themer\Repositories\Eloquent;

use Themer\Repositories\ProjectRepositoryInterface;
use Str;
use Input;
use Image;

class ProjectRepository extends BaseRepository implements ProjectRepositoryInterface{
	
	protected $model;

	public function __construct( \Themer\Models\Project $project ){
		$this->model = $project;
	}

	public function findFreeProject( $id ){
		return $this->model->wherePrice(0)->whereId($id)->firstOrFail();
	}

	public function findPremiumProject( $id ){
		return $this->model->with('software')->where('price', '>', 0)->whereId($id)->firstOrFail();
	}

	public function findProject( $type, $tag, $slug ){
		$project = $this->model->with('images')->whereHas('software',function($query)use($tag){
			$query->whereSlug($tag);
		})->whereSlug( $slug );

		if( $type == "free" )
			$project->where('price', '=', 0);
		elseif( $type == "premium" )
			$project->where('price', '>', 0);
		else return;

		return $project->firstOrFail();
	}


	public function getRandomPremium( $slug='', $num=3 ){
		if( $slug !== '' ){

			$projects = $this->model->whereHas('software', function($query)use($slug){
				$query->whereSlug( $slug );
			});

		}else{
			$projects = $this->model->with('software');
		}

		$projects->orderByRaw('RAND()')->where('price', '>', 0);

		return $projects->take($num)->get();
	}

	public function getLatestPremium(){
		return $this->model->where('price', '>', 0)->orderBy('created_at', 'desc')->take(4)->get();
	}

	public function getLatestFree(){
		return $this->model->where('price', '=', 0)->orderBy('created_at', 'desc')->take(4)->get();
	}

	public function getMostDownloaded(){
		return $this->model->where('price', '>', 0)->orderBy('downloads', 'desc')->take(4)->get();
	}

	public function getMostPopular(){

		/*
			SELECT * FROM projects
			LEFT JOIN likeable_like_counters 
			ON likeable_like_counters.likable_id = projects.id
			AND `likeable_like_counters`.likable_type = 'Themer\\Models\\Project'
			ORDER BY count DESC;
		*/

		//Seriously, wtf is with this?
		return $this->model->with('software')
	    ->leftJoin('likeable_like_counters', function ($join)
	    {
	        $join->on('likeable_like_counters.likable_id', '=', 'projects.id')
	            ->on('likeable_like_counters.likable_type', '=', \DB::raw("'Themer\\\Models\\\Project'"));
	    })
	    ->where('price', '>', 0 )
	    ->orderBy('likeable_like_counters.count', 'DESC')->take(4)->get();

	    // dd(\DB::getQueryLog());

	}


	public function paginateProjects( $free = false, $slug = '' ){
		if( $slug !== '' ){

			$projects = $this->model->whereHas('software', function($query)use($slug){
				$query->whereSlug( $slug );
			});

		}else{
			$projects = $this->model->with('software');
		}

		if( $free )
			$projects->where('price', '=', 0);
		else
			$projects->where('price', '>', 0);

		return $projects->paginate(9);
	}


	public function create( $data ){

		// To Do:
		// Project Software ID
		// Config file for directory paths

		$filename = Str::random();

		Input::file('file')->move(base_path() . '/content/', $filename );

		$project = $this->model->newInstance();
		$project->title = e($data['title']);
		$project->slug = e($data['slug']);
		$project->price = $data['price'];
		$project->body = e($data['body']);
		$project->short_desc = e($data['short_desc']);
		$project->small_image = e($data['slug']) . '_thumb.jpg';
		$project->filename = $filename;
		$project->software_id = 1;
		$project->save();

		Image::make( Input::file('images')[0]->getRealPath() )->resize(420, null,function ($constraint) {
    				$constraint->aspectRatio();
			})->save(base_path() . '/public/assets/images/themes/' . $project->small_image );

		$images = [];
		$i = 0;
		foreach( Input::file('images') as $image ){
			$file = $project->slug . '_' . $i++ . '.jpg';
			Image::make( $image->getRealPath() )->save(base_path() . '/public/assets/images/themes/' . $file);
			$images[] = new \Themer\Models\ProjectImage(['filename' => $file]);
		}

		$project->images()->saveMany( $images );

		return $project;
	}

}