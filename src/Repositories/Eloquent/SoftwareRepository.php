<?php 

namespace Themer\Repositories\Eloquent;

use Themer\Repositories\SoftwareRepositoryInterface;

class SoftwareRepository extends BaseRepository implements SoftwareRepositoryInterface{
	
	protected $model;

	public function __construct( \Themer\Models\Software $tag ){
		$this->model = $tag;
	}

	public function findAll( ){
		return $this->model->all();
	}


	public function findBySlug( $slug ){
		return $this->model->whereSlug( $slug )->first();
	}


}