<?php 

namespace Themer\Repositories\Eloquent;

use Themer\Repositories\TagRepositoryInterface;

class TagRepository extends BaseRepository implements TagRepositoryInterface{
	
	protected $model;

	public function __construct( \Themer\Models\Tag $tag ){
		$this->model = $tag;
	}

	public function findAll( ){
		return $this->model->all();
	}


	public function findBySlug( $slug ){
		return $this->model->whereSlug( $slug )->first();
	}

	public function getThreadTags(){
		$tags =$this->findAll();
		$post_tags = [];
		foreach( $tags as $tag )
			$post_tags[$tag->id] = $tag->name;

		return $post_tags;
	}


}