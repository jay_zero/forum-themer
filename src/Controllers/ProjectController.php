<?php

namespace Themer\Controllers;

use Themer\Repositories\ProjectRepositoryInterface;
use Themer\Repositories\SoftwareRepositoryInterface;
use Themer\Repositories\DownloadRepositoryInterface;
use Themer\Validators\ProjectForm;

use View;
use Input;
use Sentry;
use Session;
use Request;
use Redirect;
use Image;
use Str;

class ProjectController extends BaseController{

	protected $project, $tags, $downloads;

	protected $projectForm;

	public function __construct( ProjectRepositoryInterface $project, SoftwareRepositoryInterface $tags, DownloadRepositoryInterface $d, ProjectForm $form ){
		$this->project = $project;
		$this->tags = $tags;
		$this->downloads = $d;
		$this->projectForm = $form;
	}


/**Admin Controller for these**/
	
	public function adminAdd(){
		return View::make('admin.project.add');
	}

	public function storeProject(){

		try{
			$this->projectForm->validate( Input::all() );
		}catch(\Laracasts\Validation\FormValidationException $e){
			return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
		}

		$this->project->create( Input::all() );

		//Just whatever for now.
		return Redirect::home();

	}

/**end**/
	

	/**
	* Make this better
	*/
	public function projectsFree( $tag = '' ){

		$tag = $this->tags->findBySlug( $tag );
		$tags = $this->tags->findAll();

		$projects = $this->project->paginateProjects( true, $tag ? $tag->slug : '' );

		$premium = $this->project->getRandomPremium( $tag ? $tag->slug : '' );

		return View::make('project.list')->withProjects($projects)->withTags($tags)->withCurrent($tag)->withType('free')->withPremium($premium);
	}

	public function projectsPremium( $tag = '' ){

		$tag = $this->tags->findBySlug( $tag );
		$tags = $this->tags->findAll();

		$projects = $this->project->paginateProjects( false, $tag ? $tag->slug : '' );

		return View::make('project.list')->withProjects($projects)->withTags($tags)->withCurrent($tag)->withType('premium');
	}

	public function project( $type, $tag, $slug ){

		// dd( $slug );

		Session::put('loginRedirect', Request::url());

		$project = $this->project->findProject( $type, $tag, $slug );
		$project->increment('views');

		$links = [];

		if( $project->price > 0 ){

			//Check if the user has purchased this item
			if( Sentry::check() ){

				try{
					$download = $this->downloads->findByUserAndProjectId( Sentry::getUser()->id, $project->id );
					$downloadLink = '/download/' . $project->id . '/' . $download->token;

				}catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
					$downloadLink = '';
				}
				
			}else{
				$downloadLink = '';
			}

		}else{

			//Set generic download link
			$downloadLink = '/download/' . $project->id;

		}




		//If the project is free
		///// Show generic download link

		//If the project is NOT free
		//// Has the user already purchased?
		//////// Show unique download URL for User
		//// Show the purchase button

		return View::make('project.single')->withProject($project)->withDownloadLink( $downloadLink );
	}

}