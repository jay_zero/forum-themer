<?php

namespace Themer\Controllers;

use Illuminate\Support\Facades\Redirect;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Themer\Validators\LoginForm;
use Themer\Validators\RegisterForm;
use View;
use Event;
use Session;

class AuthController extends BaseController{

	protected $registerForm, $loginForm;

	public function __construct( LoginForm $login, RegisterForm $register ){
		$this->loginForm = $login;
		$this->registerForm = $register;
	}
	
	public function login(){
		return View::make('auth.login');
	}

	public function postLogin(){
		$input = \Input::only('username', 'password');

		try{
			$this->loginForm->validate($input);
		}catch(\Laracasts\Validation\FormValidationException $e){
			return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
		}

		try
		{
		    $user = Sentry::authenticate($input, \Input::has('remember'));
		}
		catch (\Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
			return Redirect::back()->withInput()->withErrorMessage('Invalid credentials provided');
		}
		catch (\Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
		    return Redirect::back()->withInput()->withErrorMessage('Invalid credentials provided');
		}
		catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    return Redirect::back()->withInput()->withErrorMessage('We couldn\'t find that user!');
		}
		catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
		    return Redirect::back()->withInput()->withErrorMessage('That user account has not been activated.');
		}

		// The following is only required if the throttling is enabled
		catch (\Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
		    return Redirect::back()->withInput()->withErrorMessage('User account has been suspended temporarily for too many failed log in attempts.');
		}
		catch (\Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
		    return Redirect::back()->withInput()->withErrorMessage('User account has been banned.');
		}

		$redirect = Session::get('loginRedirect', '/');

	    Session::forget('loginRedirect');

	    return Redirect::to($redirect)->withSuccessMessage('Thanks for logging in!');
	}

	public function logout(){
		Sentry::logout();
		Session::forget('loginRedirect');
		return Redirect::to('/')->withSuccessMessage('Successfully logged out of your account.');
	}

	public function getRegister(){
		return View::make('auth.register');
	}

	public function postRegister(){

		$input = \Input::only('username', 'password', 'first_name', 'last_name', 'email');

		try{
			$this->registerForm->validate(\Input::only('username', 'password', 'password_confirmation', 'first_name', 'last_name', 'email'));
		}catch(\Laracasts\Validation\FormValidationException $e){
			return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
		}

	    $user = Sentry::register( $input );

		Event::fire('UserHasSignedUp', [ $user ] );

	    return Redirect::to('');


	}

	public function getActivate($user_id, $code){
		try
		{
		    // Find the user using the user id
		    $user = Sentry::findUserById($user_id);

		    // Attempt to activate the user
		    if ($user->attemptActivation($code)){
		        Sentry::login($user, false);
		        Event::fire('UserHasActivated', [$user]);
		        return Redirect::to('/')->withSuccessMessage('Welcome back!');
		    }else{
		        return Redirect::to('/')->withErrorMessage('The activation code was wrong.');
		    }
		}
		catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    return Redirect::to('/')->withErrorMessage('The user was not found!');
		}
		catch (\Cartalyst\Sentry\Users\UserAlreadyActivatedException $e)
		{
		    return Redirect::to('/login')->withErrorMessage('The is user is already activated, please login to continue.');
		}
	}

}