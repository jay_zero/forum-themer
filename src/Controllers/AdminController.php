<?php

namespace Themer\Controllers;
use Themer\Repositories\OrderRepositoryInterface;
use Themer\Repositories\UserRepositoryInterface;

use View;
use Input;
use Redirect;

/**
 * This is temporary.  Probably anything that goes here can be moved out to the individual controllers.  Just testing stuff.
 */

class AdminController extends BaseController{

	protected $order;

	public function __construct( OrderRepositoryInterface $order, UserRepositoryInterface $user ){
		$this->order 	= $order;
		$this->user 	= $user;
	}

	public function showHome(){

		//Maybe theres a better way for this?
		$stats = $this->order->getStats();
		$stats->users = $this->user->getMemberCount();

		$orders = $this->order->getLatest();
		return View::make('admin.dashboard')->withOrders( $orders )->withStats($stats);
	}

}