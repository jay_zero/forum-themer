<?php

namespace Themer\Controllers;

use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Themer\Models\Notification;
use Response;
use Request;

class NotificationController extends BaseController{

	public function index(){

		$timeout = 0;

		if( !Sentry::check() ) return Response::json(['stop'=>true]);

		$messages=$this->check_events();

		\DB::table('notifications')->whereUserId( Sentry::getUser()->id )->update(['served'=>1]);

		if( $messages )
			return Response::json(['stop'=>false, 'messages'=>$messages->toArray()]);
		else
			return Response::json(['stop'=>false, 'messages'=>[]]);

	}

	//Move this to a repository, it doesn't belong here
	private function check_events(){
		$messages = Notification::whereServed(0)->whereUserId( Sentry::getUser()->id )->get();

		if( $messages->count() == 0 ) return false;
		else return $messages;
	}

	public function fire(){
		$not = new Notification();
		$not->message = 'This is a new notification from the test route';
		$not->user_id = 3;
		$not->save();
	}

}