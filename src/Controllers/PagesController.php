<?php

namespace Themer\Controllers;

use View;
use Input;
use Redirect;

class PagesController extends BaseController{

	protected $projects;

	public function __construct( \Themer\Repositories\ProjectRepositoryInterface $project ){
		$this->projects = $project;
	}

	public function showHome(){
		$free = $this->projects->getLatestFree();

		$premium = $this->projects->getLatestPremium(); 
		$downloaded = $this->projects->getMostDownloaded(); 
		$popular = $this->projects->getMostPopular();

		return View::make('pages.home')->with([
				'free'			=>	$free,
				'latestPremium'	=>	$premium,
				'downloaded'	=>	$downloaded,
				'popular'		=>	$popular
			]);
	}

	public function showPrivacy(){
		return View::make('pages.privacy');
	}

	public function showContact(){
		return View::make('pages.contact');
	}
}