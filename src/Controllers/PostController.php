<?php

namespace Themer\Controllers;

use Themer\Repositories\PostRepositoryInterface;
use Themer\Repositories\TagRepositoryInterface;
use Themer\Validators\ThreadForm;
use Themer\Validators\PostForm;
use Themer\Models\Post;

use View;
use Input;
use Redirect;
use Lang;

class PostController extends BaseController{

	protected $post, $tags, $threadForm, $postForm, $stats;

	public function __construct( PostRepositoryInterface $post, TagRepositoryInterface $tags, 
								 ThreadForm $thread, PostForm $postForm ){
		$this->post = $post;
		$this->tags = $tags;
		$this->threadForm = $thread;
		$this->postForm = $postForm;

		$this->stats = $this->post->getStats();
	}
	

	public function index(){
		
		$tag = $this->tags->findBySlug( Input::get('tag') );

		$posts = $this->post->paginateTopics( $tag ? $tag->slug : '' );

		$tags = $this->tags->findAll();

		return View::make('forum.posts')->withPosts($posts)->withTags($tags)->withStats( $this->stats );
	}

	public function thread( $threadId ){
		$post = $this->post->findById( $threadId );
		$post->replies = $this->post->paginateReplies( $threadId );

		$tags = $this->tags->findAll();

		$post_tags = [];
		foreach( $post->tags as $tag ) $post_tags[] = $tag->id;
		$post->active = $post_tags;

		return View::make('forum.thread')->withPost($post)->withTags($tags);
	}


	public function createThread(){
		$tags = $this->tags->getThreadTags();
		return View::make('forum.form.thread')->withTags($tags);
	}

	public function storeThread(){

		try{
			$this->threadForm->validate( Input::all() );
		}catch(\Laracasts\Validation\FormValidationException $e){
			return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
		}


		//Move this to Form->getData would be better.
		$threadData = Input::only('title', 'body');
		$threadData['user_id'] = \Sentry::getUser()->id;
		$threadData['last_post'] = date('Y-m-d H:i:s');
		$threadData['tags'] = Input::get('tags');

		$post = $this->post->create( $threadData );


		return Redirect::route('forum.thread', $post->id );

	}


	public function createPost( $thread_id ){
		$post = $this->post->findById( $thread_id );

		if( $post->closed && !\Sentry::getUser()->hasAnyAccess(['moderate', 'admin']) )
			return Redirect::route('forum.thread', $post->id )->withErrorMessage( Lang::get('forum.error.closed') );

		return View::make('forum.form.post')->withThreadId( $thread_id );
	}


	public function storePost( $thread_id ){

		$post = $this->post->findById( $thread_id );

		if( $post->closed && !\Sentry::getUser()->hasAnyAccess(['moderate', 'admin']) )
			return Redirect::route('forum.thread', $post->id )->withErrorMessage( Lang::get('forum.error.closed') );

		try{
			$this->postForm->validate( Input::all() );
		}catch(\Laracasts\Validation\FormValidationException $e){
			return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
		}


		//Move this to Form->getData would be better.
		$threadData = Input::only('body');
		$threadData['user_id'] = \Sentry::getUser()->id;
		$threadData['last_post'] = date('Y-m-d H:i:s');
		$threadData['parent'] = $post->id;

		$reply = $this->post->create( $threadData );


		\Event::fire('NewReplyInThread', [ $reply ] );

		$page = ceil( ($post->replies->count() +1) / 15 );

		return Redirect::route('forum.thread', [$reply->parent, 'page'=>$page] );

	}

	public function showEdit( $id ){
		$post = $this->post->findPostById( $id );

		if( !\Sentry::getUser()->hasAnyAccess(['moderate', 'admin']) && \Sentry::getUser()->id != $post->user->id )
			return Redirect::route('forum.index')->withErrorMessage( Lang::get('global.error.permission') );

		return View::make('forum.form.edit')->withPost($post);
	}

	public function storeEdit( $id ){
		$post = $this->post->findPostById( $id );

		if( !\Sentry::getUser()->hasAnyAccess(['moderate', 'admin']) && \Sentry::getUser()->id != $post->user->id )
			return Redirect::route('forum.index')->withErrorMessage( Lang::get('global.error.permission') );

		try{
			$this->postForm->validate( Input::all() );
		}catch(\Laracasts\Validation\FormValidationException $e){
			return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
		}


		$this->post->savePost( $post, Input::only('body') );

		$return = $post->parent == 0 ? $post->id : $post->parent;

		return Redirect::route('forum.thread', [$return] )->withSuccessMessage( Lang::get('forum.success.edit') );
	}


	public function closeThread( $id ){
		$post = $this->post->closeThreadById( $id );
		return Redirect::route('forum.thread', $post->id );
	}

	public function deleteThread( $id ){
		$this->post->deleteThreadById( $id );
		return Redirect::route('forum.index')->withSuccessMessage( Lang::get('forum.success.delete') );
	}


	public function deletePost( $id ){
		$post = $this->post->findPostById( $id );
		$thread = $post->parent;

		if( \Sentry::getUser()->id != $post->user_id && !\Sentry::getUser()->hasAnyAccess(['admin','moderate']))
			return Redirect::route('forum.thread', $post->parent)->withErrorMessage( Lang::get('global.error.permission') );

		if( $post->parent == 0)
			return Redirect::route('forum.thread', $post->id)->withErrorMessage( Lang::get('global.error.permission') );

		$post->delete();

		return Redirect::route('forum.thread', $thread )->withSuccessMessage( Lang::get('forum.success.delete') );
	}

	public function like( $id ){

		//I'd like to make this more generic for any type of "likeable"

		$like = true;

		$post = $this->post->findPostById( $id );

		if( $post->liked( \Sentry::getUser()->id ) ){
			$post->unlike( \Sentry::getUser()->id );
			$like = false;
		}
		else {
			$post->like( \Sentry::getUser()->id );
		}

		\Event::fire('PostLiked', [$post]);

		if( \Request::ajax() ){
			if( $post->likes == 1 )
    			return \Response::json( [ 'likes' => sprintf( Lang::get('forum.like.single'), $post->likes ), 'verb' => $like?'Unlike':'Like' ] );
    		elseif( $post->likes > 1 )
    			return \Response::json( [ 'likes' => sprintf( Lang::get('forum.like.plural'), $post->likes ), 'verb' => $like?'Unlike':'Like' ] );
    		else
    			return \Response::json( [ 'likes' => '', 'verb' => $like?'Unlike':'Like' ] );
		}else{
			return Redirect::back()->withSuccessMessage( "Post Liked!" ); 
		}

	}

}