<?php

namespace Themer\Controllers;

use Themer\Validators\PasswordForm;
use Themer\Validators\InfoForm;
use Themer\Validators\AvatarForm;
use Themer\Repositories\OrderRepositoryInterface;
use Themer\Repositories\DownloadRepositoryInterface;

use View;
use Sentry;
use Input;
use Redirect;
use Image;

class AccountController extends BaseController{

	protected $passwordForm, $infoForm, $order, $download, $avatarForm;

	public function __construct( PasswordForm $password, InfoForm $info, OrderRepositoryInterface $order, DownloadRepositoryInterface $dl,AvatarForm $avatar ){ 
		$this->passwordForm = $password; 
		$this->infoForm = $info;
		$this->order = $order; 
		$this->download = $dl;
		$this->avatarForm = $avatar;
	}

	public function dashboard(){
		return View::make('account.dashboard');
	}

	public function showProfile( $id ){


		$likes = \Themer\Models\User::rep($id);

		$user = \Themer\Models\User::with(['posts'=>function($query){
			$query->orderBy('created_at', 'DESC')->take(10);
		}])->find($id);
		return View::make('account.profile')->withUser($user)->withLikes($likes);
	}

	public function transactions(){
		$transactions = $this->order->paginateByUserId( Sentry::getUser()->id );
		return View::make('account.transactions')->withTransactions( $transactions );
	}

	public function downloads(){
		$downloads = $this->download->paginateByUserId( Sentry::getUser()->id );
		return View::make('account.downloads')->withDownloads( $downloads );
	}

	public function showAvatar(){
		return View::make('account.avatar');
	}

	public function storeAvatar(){

		try{
			$this->avatarForm->validate( Input::all() );
		}catch(\Laracasts\Validation\FormValidationException $e){
			return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
		}

		if( Input::has( 'remove_avatar' ) ){
			Sentry::getUser()->avatar = null;
	   		Sentry::getUser()->save();
		}else{

			$img = Image::make( Input::file('avatar')->getRealPath() )->fit(120, 120);

			$img->save(base_path() . '/public/uploads/avatar-' . Sentry::getUser()->id .'.png');

		   	Sentry::getUser()->avatar = 'avatar-' . Sentry::getUser()->id .'.png';
		   	Sentry::getUser()->save();

		}

	    return Redirect::route('account.dashboard')->withSuccessMessage('Your avatar has been successfully updated.');
	}


	public function showPassword(){
		return View::make('account.password');
	}

	public function storePassword(){

		if( !Sentry::getUser()->checkPassword( Input::get('old_password') ) ){
			return Redirect::back()->withInput()->withErrors( ['old_password'=>'You entered an incorrect password.'] );
		}


		try{
			$this->passwordForm->validate( Input::only('new_password', 'new_password_confirmation') );
		}catch(\Laracasts\Validation\FormValidationException $e){
			return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
		}

	   	Sentry::getUser()->password = Input::get('new_password');
	   	Sentry::getUser()->save();

		\Event::fire('PasswordChanged', [ Sentry::getUser() ] );

	    return Redirect::route('account.dashboard')->withSuccessMessage('Your password has been successfully updated.');
	}


	public function showInformation(){
		return View::make('account.information')->withUser(Sentry::getUser());
	}

	public function storeInformation(){

		if( !Sentry::getUser()->checkPassword( Input::get('password') ) ){
			return Redirect::back()->withInput()->withErrors( ['password'=>'You entered an incorrect password.'] );
		}

		try{
			$this->infoForm->validate( Input::only('email', 'first_name', 'last_name') );
		}catch(\Laracasts\Validation\FormValidationException $e){
			return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
		}

		$user = Sentry::getUser();
		$user->first_name = Input::get('first_name');
		$user->last_name = Input::get('last_name');
		$user->email = Input::get('email');
		$user->email_updates = Input::has('email_updates');
	   	$user->save();

		\Event::fire('InformationUpdated', [ Sentry::getUser() ] );

	    return Redirect::route('account.dashboard')->withSuccessMessage('Your password has been successfully updated.');
	}


}