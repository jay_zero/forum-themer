<?php

namespace Themer\Controllers;

use Themer\Repositories\DownloadRepositoryInterface;
use Themer\Repositories\ProjectRepositoryInterface;
use Sentry;
use App;
use Config;

class DownloadController extends BaseController{

	protected $project, $download;

	public function __construct( DownloadRepositoryInterface $download, ProjectRepositoryInterface $project ){
		$this->download = $download;
		$this->project = $project;
	}


	public function download( $project_id, $token = '' ){


		if( $token == '' ){

			try{
				$project = $this->project->findFreeProject( $project_id );


				$file = Config::get('app.downloads') . $project->filename;
				
				if( $project->filename && file_exists( $file ) && !is_dir( $file ) ){
					$project->increment('downloads');
					return \Response::make( file_get_contents($file), 200, ['Content-Type'=>'application/x-zip', 'Content-Disposition'=> 'attachment; filename="forumthemer-'.$project->slug.'-'.$project->id.'.zip"']);
				}else{
					App::abort(404);
				}

			}catch( \Illuminate\Database\Eloquent\ModelNotFoundException $e ){
				App::abort(404);
			}

		}else{
		
			try{	

				$download = $this->download->findByUserAndToken( Sentry::getUser()->id, $project_id, $token );

				$file = Config::get('app.downloads') . $download->project->filename;

				if( $download->project->filename && file_exists( $file ) && !is_dir( $file ) ){
					$download->project()->increment('downloads');

					return \Response::make( file_get_contents($file), 200, ['Content-Type'=>'application/x-zip', 'Content-Disposition'=> 'attachment; filename="FT-'.$download->project->slug.'-'.$download->project->id.'.zip"']);
				}else{
					App::abort(404);
				}


			}catch( \Illuminate\Database\Eloquent\ModelNotFoundException $e ){
				return \Redirect::to('/')->withErrorMessage('The download you requested does not exist for the current user.  Please make sure you\'re logged in to the correct account and try again' );
			}


		}

	}

}