<?php

namespace Themer\Controllers;

use Themer\Repositories\OrderRepositoryInterface;
use Themer\Repositories\OrderLineRepositoryInterface;
use Themer\Repositories\ProjectRepositoryInterface;
use Themer\Repositories\DownloadRepositoryInterface;
use Themer\Validators\ThreadForm;
use Themer\Validators\PostForm;
use Themer\Validators\RegisterForm;
use Themer\Models\Post;

use View;
use Input;
use Redirect;

use Stripe;
use Stripe_Charge;
use Stripe_CardError;
use Config;
use Cart;
use Session;

class OrderController extends BaseController{

	protected $project, $download, $order, $orderLine;

	public function __construct( ProjectRepositoryInterface $project, DownloadRepositoryInterface $download, OrderRepositoryInterface $order, OrderLineRepositoryInterface $ol, RegisterForm $form ){
		$this->project = $project;
		$this->download = $download;
		$this->order = $order;
		$this->orderLine = $ol;
		$this->registerForm = $form;
	}

	public function success(){
		if( !Session::has('order_id') ) return Redirect::home();

		$order = $this->order->findById( Session::get('order_id') , \Sentry::getUser()->id );

		Session::forget('order_id');

		return View::make('order.success')->withOrder($order);
	}



	public function showOrderForm( ){

		if( Cart::count() < 1 ){
			return Redirect::route('cart.index');
		}

		return View::make('order.form');
	}


	public function processOrder( ){


		if( !\Sentry::check() ){
			
			$input = \Input::only('username', 'password', 'first_name', 'last_name', 'email');

			try{
				$this->registerForm->validate(\Input::only('username', 'password', 'password_confirmation', 'first_name', 'last_name', 'email'));
			}catch(\Laracasts\Validation\FormValidationException $e){
				return Redirect::back()->withInput()->withErrors( $e->getErrors() );	
			}

		    $user = \Sentry::register( $input, true );

			\Event::fire('UserHasActivated', [ $user ] );

			\Sentry::login( $user, false );

		}



		if( Cart::count() < 1 ){
			return;
		}

		$subtotal = 0;

		foreach( Cart::content() as $item ){
			$project = $this->project->findPremiumProject( $item->id );
			$subtotal += $project->price;

			if( $item->options->has('copyright') )
				$subtotal += Config::get('app.copyright_cost'); 
		}

		//Do Stripe work
		Stripe::setApiKey(Config::get('laravel-stripe::stripe.api_key'));

		// Get the credit card details submitted by the form
		$token = Input::get('stripeToken');

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
			$charge = Stripe_Charge::create(array(
			  "amount" => $subtotal, // amount in cents, again
			  "currency" => "gbp",
			  "card" => $token,
			  "description" => "Forum Themer Purchase")
			);

			//Create a service for this which will make the constructor for this controller better >_>
			$order = $this->order->saveOrder( \Sentry::getUser()->id, $charge->id, $subtotal );

			foreach( Cart::content() as $item ){
				// $project = $this->project->findPremiumProject( $item->id );
				$this->orderLine->createOrderLine( $order->id, $item );
				$this->download->generateNewToken( \Sentry::getUser()->id, $item->id );
			}

			\Event::fire('UserTransactionComplete', ['user'=>\Sentry::getUser(), 'items' => Cart::content()]);

			Cart::destroy();


			Session::put('order_id', $order->id);
			return Redirect::route('order.success');
			// return Redirect::route('project.single', ['premium', $project->software->slug, $project->slug]);

		} catch(Stripe_CardError $e) {
		  // The card has been declined
			die('WHAT? Your card failed -_-');
		}

	}

}