<?php

namespace Themer\Controllers;

use Themer\Repositories\PostRepositoryInterface;
use Themer\Repositories\TagRepositoryInterface;
use Themer\Validators\ThreadForm;
use Themer\Validators\PostForm;
use Themer\Models\Post;
use Themer\Models\Project;

use View;
use Input;
use Redirect;
use Lang;

class LikeController extends BaseController{

	// public function __construct( LikeRepositoryInterface $like ){

	// }


	public function store( $pid ){
	
		//temp
		$post = Post::find( $pid );
		$post->like( \Sentry::getUser()->id );

	} 

	public function storeTheme( $pid ){
	
		//temp
		$post = Project::find( $pid );
		$post->like( \Sentry::getUser()->id );

	} 

	public function destroy( $pid ){

	}

}