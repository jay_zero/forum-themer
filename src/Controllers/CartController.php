<?php

namespace Themer\Controllers;

use Themer\Repositories\ProjectRepositoryInterface;

use Cart;
use Redirect;
use View;
use Config;
use Lang;

class CartController extends BaseController{

	protected $project;

	public function __construct( ProjectRepositoryInterface $p ){
		$this->project = $p;
	}

	public function add( $id ){
		$project = $this->project->findPremiumProject($id);

		if( Cart::search(['id'=>$id]) )
			return Redirect::route('cart.index')->withErrorMessage(Lang::get('cart.error.duplicate'));

		Cart::add($project->id, $project->title, 1, $project->price);

		return Redirect::route('cart.index')->withSuccessMessage(sprintf(Lang::get('cart.success.add'), $project->title));
	}

	public function cart_empty(){
		Cart::destroy();

		return Redirect::route('cart.index')->withSuccessMessage(Lang::get('cart.success.empty'));
	}

	public function remove( $id ){
		Cart::remove($id);

		return Redirect::route('cart.index')->withSuccessMessage(Lang::get('cart.success.update'));
	}

	public function cart(){
		return View::make('cart.show');
	}

	public function copyrightService($id){

		$item = Cart::get($id);
		$project = $this->project->findPremiumProject($item->id);

		if( $item->options->has('copyright') ){
			Cart::update($id, array('price' => $project->price));
			$item->options->forget('copyright');
		}else{
			Cart::update($id, array('price' => $project->price + Config::get('app.copyright_cost'), 'options' => ['copyright'=>true]));
		}

		return Redirect::route('cart.index')->withSuccessMessage(Lang::get('cart.success.update'));
	}

}