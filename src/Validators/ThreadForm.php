<?php

namespace Themer\Validators;

use Laracasts\Validation\FormValidator;

class ThreadForm extends FormValidator{
	

	protected $rules = [
		'title' 		=> 'required|min:10',
		'body'	 		=> 'required|min:10',
		'tags'			=> 'required|each:exists,forum_tags,id'
	];

	protected $messages = [
		'each' => 'One or more :attribute you supplied do not exist'
	];

}