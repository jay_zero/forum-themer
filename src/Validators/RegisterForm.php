<?php

namespace Themer\Validators;

use Laracasts\Validation\FormValidator;

class RegisterForm extends FormValidator{
	

	protected $rules = [
		'username' 		=> 'required|unique:users',
		'password' 		=> 'required|confirmed|min:8',
		'email'			=> 'required|email|unique:users',
		'first_name'	=> 'required',
		'last_name'		=> 'required'
	];

}