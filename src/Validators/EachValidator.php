<?php

namespace Themer\Validators;

class EachValidator extends \Illuminate\Validation\Validator {
  
  public function validateEach($attribute, $values, $parameters)
  {
    $rule = array_shift($parameters);
    
    $method = 'validate' . studly_case($rule);
    
    foreach ($values as $value)
    {
      if (! $this->{$method}($attribute, $value, $parameters))
      {
        return false;
      }
    }
    
    return true;
  }
}