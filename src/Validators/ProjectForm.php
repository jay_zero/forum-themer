<?php

namespace Themer\Validators;

use Laracasts\Validation\FormValidator;

class ProjectForm extends FormValidator{
	
	protected $rules = [
		'images'	 		=> 'required|each:image',
		'file'				=> 'required',
		'title' 			=> 'required',
		'body'	 			=> 'required|min:10',
		'short_desc'		=> 'required|min:10',
		'slug'				=> 'required',
		'price'				=> 'required|integer'
	];

	//Something something something.
}