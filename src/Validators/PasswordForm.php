<?php

namespace Themer\Validators;

use Laracasts\Validation\FormValidator;

class PasswordForm extends FormValidator{
	

	protected $rules = [
		'new_password' 		=> 'required|confirmed|min:8'
	];

}