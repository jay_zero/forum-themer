<?php

namespace Themer\Validators;

use Laracasts\Validation\FormValidator;

class PostForm extends FormValidator{
	
	protected $rules = [
		'body'	 		=> 'required|min:10'
	];
}