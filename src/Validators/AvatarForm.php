<?php

namespace Themer\Validators;

use Laracasts\Validation\FormValidator;

class AvatarForm extends FormValidator{
	
	protected $rules = [
		'avatar'	 		=> 'required_without:remove_avatar|image|max:100|mimes:jpeg,png'
	];

	//Something something something.
}