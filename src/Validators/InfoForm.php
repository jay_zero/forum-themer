<?php

namespace Themer\Validators;

use Laracasts\Validation\FactoryInterface as ValidatorFactory;
use Laracasts\Validation\FormValidator;
use Sentry;

class InfoForm extends FormValidator{
	

	protected $rules = [
		'first_name'	=> 'required',
		'last_name'		=> 'required'
	];

	public function __construct( ValidatorFactory $v ){
		if( Sentry::check() )
			$this->rules['email'] = 'required|email|unique:users,email,'.Sentry::getUser()->id;
		
		parent::__construct( $v );
	}

}