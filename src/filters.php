<?php

Route::filter( 'admin', function(){

	if( !Sentry::getUser()->hasAnyAccess( ['admin'] ) ){
		return Redirect::to('/')->withErrorMessage('You don\'t have permission to perform that action.');
	}

});

Route::filter( 'moderate', function(){

	if( !Sentry::getUser()->hasAnyAccess( ['moderate', 'admin'] ) ){
		return Redirect::to('/')->withErrorMessage('You don\'t have permission to perform that action.');
	}

});

Route::filter( 'authenticated', function(){

	if( !Sentry::check() ){
		return Redirect::route('auth.login')->withErrorMessage('You must login to perform that action.');
	}

});


Route::filter( 'notauthenticated', function(){

	if( Sentry::check() ){
		return Redirect::route('auth.login')->withErrorMessage('You are already logged in.  To perform that action please logout.');
	}

});