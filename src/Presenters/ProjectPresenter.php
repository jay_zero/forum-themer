<?php 

namespace Themer\Presenters;

use Laracasts\Presenter\Presenter;
use Markdown;

class ProjectPresenter extends Presenter
{
	public function price_actual(){
        return '&pound;' . number_format($this->price / 100, 2);
    }

    public function description(){
    	return Markdown::render( $this->body );
    }
}