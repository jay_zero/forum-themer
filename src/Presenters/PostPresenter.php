<?php 

namespace Themer\Presenters;

use Laracasts\Presenter\Presenter;
use Illuminate\Support\Facades\HTML;
use Carbon\Carbon;
use BBCode;
use Lang;

class PostPresenter extends Presenter
{

	public function bb_body(){
		return BBCode::parse( nl2br($this->body) );
	}
    public function last_update(){
    	return $this->updated_at->diffForHumans();
    }
    public function last_post_date(){
    	return (new Carbon($this->last_post))->diffForHumans();
    }
    public function meta(){
    	return sprintf( Lang::get('forum.thread.meta'), $this->user->present()->user_link, $this->last_post_date );
    }
    public function post_meta(){
    	return sprintf( Lang::get('forum.post.meta'), $this->last_update );
    }
}