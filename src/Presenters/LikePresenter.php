<?php 

namespace Themer\Presenters;

use Laracasts\Presenter\Presenter;
use Illuminate\Support\Facades\HTML;
use Carbon\Carbon;
use BBCode;
use Lang;

class LikePresenter extends Presenter
{
    public function numLikes(){
        if( $this->count() == 1 )
    	   return Lang::get('forum.likes.single');
        else return sprintf( Lang::get('forum.likes.plural'), $this->count() );
    }
}