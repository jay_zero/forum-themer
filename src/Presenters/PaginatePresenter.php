<?php

namespace Themer\Presenters;

class PaginatePresenter extends \Illuminate\Pagination\Presenter {

    public function getActivePageWrapper($text)
    {
        return '<li><a href="" class="active">'.$text.'</a></li>';
    }

    public function getDisabledTextWrapper($text)
    {
        return '<li class="unavailable"><a href="">'.$text.'</a></li>';
    }

    public function getPageLinkWrapper($url, $page, $rel = null)
    {
        return '<li><a href="'.$url.'">'.$page.'</a></li>';
    }

    public function getPrevious($text = '&laquo;'){
        return parent::getPrevious($text = 'Previous');
    }

    public function getNext($text = '&laquo;'){
        return parent::getNext($text = 'Next');
    }
}