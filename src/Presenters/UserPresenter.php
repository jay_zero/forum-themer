<?php 

namespace Themer\Presenters;

use Laracasts\Presenter\Presenter;
use Illuminate\Support\Facades\HTML;
use Carbon\Carbon;
use URL;

class UserPresenter extends Presenter
{

    public function user_link(){
    	return link_to_route('account.profile', $this->username, $this->id);
    }

    public function avatar_image(){

    	if( $this->avatar == '' )
    		return HTML::image( URL::asset('assets/images/no-avatar.png'), $this->username );
    	else
    		return HTML::image( URL::asset('uploads/' . $this->avatar), $this->username );
    }

    public function join_date(){
    	return $this->created_at->diffForHumans();
    }


}