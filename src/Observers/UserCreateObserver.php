<?php

namespace Themer\Observers;
use Mail;

class UserCreateObserver {
	
	public function handle( $user ){

		Mail::send('emails.activate', array('activation' => $user->getActivationCode(), 'user_id' => $user->id, 'username' => $user->username), function($message) use ($user)
		{
	    	$message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Account Activation Required');
		});

	}

}