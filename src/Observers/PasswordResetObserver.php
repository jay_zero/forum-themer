<?php

namespace Themer\Observers;
use Mail;

class PasswordResetObserver {
	
	public function handle( $user ){

		Mail::send('emails.reset', array('username' => $user->username, 'code' => $user->getResetPasswordCode() ), function($message) use ($user)
		{
	    	$message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Password Reset Code');
		});

	}

}