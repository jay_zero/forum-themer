<?php

namespace Themer\Observers;
use Mail;

class UserActivateObserver {
	
	public function handle( $user ){

		Mail::send('emails.welcome', array('username' => $user->username), function($message) use ($user)
		{
	    	$message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Welcome to Forum Themer!');
		});

	}

}