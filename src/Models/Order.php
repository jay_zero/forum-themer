<?php

namespace Themer\Models;

class Order extends BaseModel{

	protected $fillable = [ 'stripeToken', 'total', 'user_id' ];

	public function project(){
		return $this->belongsTo( '\Themer\Models\Project' );
	}

	public function lines(){
		return $this->hasMany( '\Themer\Models\OrderLine' );
	}

	public function user(){
		return $this->belongsTo( '\Themer\Models\User' );
	}

	public function getOrderTotalAttribute(){
		return $this->sum('total') / 100;
	}

}