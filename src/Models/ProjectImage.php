<?php

namespace Themer\Models;

use Laracasts\Presenter\PresentableTrait;

class ProjectImage extends BaseModel{

	protected $fillable = ['filename'];

	public function project(){
		return $this->belongsTo('\Themer\Models\Project');
	}
}