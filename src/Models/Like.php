<?php

namespace Themer\Models;

use Laracasts\Presenter\PresentableTrait;

class Like extends BaseModel{

	use PresentableTrait;

	protected $presenter = 'Themer\Presenters\LikePresenter';

	protected $table = 'post_likes';

}