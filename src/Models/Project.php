<?php

namespace Themer\Models;

use Laracasts\Presenter\PresentableTrait;

class Project extends BaseModel{

	use PresentableTrait;
	use \Conner\Likeable\LikeableTrait;

	protected $presenter = 'Themer\Presenters\ProjectPresenter';

	public function images(){
		return $this->hasMany('\Themer\Models\ProjectImages');
	}

	public function software(){
		return $this->belongsTo('\Themer\Models\Software');
	}
}