<?php

namespace Themer\Models;

class Download extends BaseModel{

	protected $fillable = ['user_id', 'project_id', 'token'];

	public function user(){
		return $this->belongsTo('\Themer\Models\User');
	}

	public function project(){
		return $this->belongsTo('\Themer\Models\Project');
	}

}