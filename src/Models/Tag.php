<?php

namespace Themer\Models;

class Tag extends BaseModel{

	protected $table = 'forum_tags';

	public function posts(){
		return $this->belongsToMany('\Themer\Models\Post', 'forum_tags_pivot', 'tag_id', 'post_id');
	}

}