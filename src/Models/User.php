<?php

namespace Themer\Models;

use Laracasts\Presenter\PresentableTrait;
use DB;

class User extends \Cartalyst\Sentry\Users\Eloquent\User{

	use PresentableTrait;

	protected $presenter = 'Themer\Presenters\UserPresenter';

	public function orders(){
		return $this->hasMany( '\Themer\Models\Order' );
	}

	public function posts(){
		return $this->hasMany( '\Themer\Models\Post' );
	}

	public static function rep( $id ){

		//Unfortunate way of doing this but it's at least in the model and cached
		//Better than the logic in the controller.

		//Needs moving to Repository though

		return DB::table('likeable_like_counters')
					->select( DB::raw('SUM(count) as rep') )
					->leftJoin( 'posts', function( $join ){
						$join->on( 'likeable_like_counters.likable_id', '=', 'posts.id')
						->on( 'likeable_like_counters.likable_type', '=', DB::raw('"Themer\\\Models\\\Post"') );
					})->whereUserId( $id )->remember(10)->first()->rep;

	}

}