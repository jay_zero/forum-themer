<?php

namespace Themer\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model{

	use \SoftDeletingTrait;

}