<?php

namespace Themer\Models;

class OrderLine extends BaseModel{

	protected $table = "order_lines";

	protected $fillable = [ 'item_name', 'item_desc', 'item_cost', 'project_id', 'copyright_cost', 'copyright', 'order_id' ]; //Add Item Actual Cost
	
	public function order(){
		return $this->belongsTo( '\Themer\Models\Order');
	}

	public function download(){
		return $this->hasOne( '\Themer\Models\Download', 'project_id', 'project_id');
	}

	public function project(){
		return $this->hasOne( '\Themer\Models\Project', 'id', 'project_id');
	}

}