<?php

namespace Themer\Models;

use Laracasts\Presenter\PresentableTrait;

class Post extends BaseModel{

	use \Conner\Likeable\LikeableTrait;
	use PresentableTrait;

	protected $fillable = [ 'title', 'body', 'user_id', 'parent', 'last_post'];

	protected $presenter = 'Themer\Presenters\PostPresenter';

	public function user(){
		return $this->belongsTo( '\Themer\Models\User' );
	}

	public function replies(){
		return $this->hasMany( '\Themer\Models\Post', 'parent' );
	}

	public function tags(){
		return $this->belongsToMany('\Themer\Models\Tag', 'forum_tags_pivot', 'post_id', 'tag_id');
	}

	public function likesForUser( $uid ){
		return $this->whereUserId( $uid )->likes;
	}


	// public function setBodyAttribute($value)
 //    {
 //        $this->attributes['body'] = htmlentities($value);
 //    }

}