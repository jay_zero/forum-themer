<?php

Event::Listen( 'UserHasSignedUp', '\Themer\Observers\UserCreateObserver' );
Event::Listen( 'UserHasActivated', '\Themer\Observers\UserActivateObserver' );