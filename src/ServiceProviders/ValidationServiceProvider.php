<?php 

namespace Themer\ServiceProviders;
 
use Illuminate\Support\ServiceProvider;
use \Themer\Validators\EachValidator;
 
class ValidationServiceProvider extends ServiceProvider {
 
  public function register(){}
 
  public function boot()
  {
    $this->app->validator->resolver(function($translator, $data, $rules, $messages)
    {
      return new EachValidator($translator, $data, $rules, $messages);
    });
  }
 
}