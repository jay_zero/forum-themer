<?php

namespace Themer\ServiceProviders;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind('Themer\Repositories\PostRepositoryInterface', 'Themer\Repositories\Eloquent\PostRepository');
        $this->app->bind('Themer\Repositories\ProjectRepositoryInterface', 'Themer\Repositories\Eloquent\ProjectRepository');
        $this->app->bind('Themer\Repositories\TagRepositoryInterface', 'Themer\Repositories\Eloquent\TagRepository');
        $this->app->bind('Themer\Repositories\SoftwareRepositoryInterface', 'Themer\Repositories\Eloquent\SoftwareRepository');
        $this->app->bind('Themer\Repositories\DownloadRepositoryInterface', 'Themer\Repositories\Eloquent\DownloadRepository');
        $this->app->bind('Themer\Repositories\OrderRepositoryInterface', 'Themer\Repositories\Eloquent\OrderRepository');
        $this->app->bind('Themer\Repositories\OrderLineRepositoryInterface', 'Themer\Repositories\Eloquent\OrderLineRepository');
        $this->app->bind('Themer\Repositories\UserRepositoryInterface', 'Themer\Repositories\Eloquent\UserRepository');

    }
}
