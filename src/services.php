<?php

App::bind('Themer\Repositories\PostRepositoryInterface', 'Themer\Repositories\Eloquent\PostRepository');
App::bind('Themer\Repositories\ProjectRepositoryInterface', 'Themer\Repositories\Eloquent\ProjectRepository');
App::bind('Themer\Repositories\TagRepositoryInterface', 'Themer\Repositories\Eloquent\TagRepository');
App::bind('Themer\Repositories\SoftwareRepositoryInterface', 'Themer\Repositories\Eloquent\SoftwareRepository');
App::bind('Themer\Repositories\DownloadRepositoryInterface', 'Themer\Repositories\Eloquent\DownloadRepository');
App::bind('Themer\Repositories\OrderRepositoryInterface', 'Themer\Repositories\Eloquent\OrderRepository');
App::bind('Themer\Repositories\OrderLineRepositoryInterface', 'Themer\Repositories\Eloquent\OrderLineRepository');