# Forum Themer

The source code for [Forum Themer](http://www.forumthemer.net) built with Laravel.  See `composer.json` for the required packages for this application.

### Install the software locally

If you're using the gruntfile (which might yet change to gulp) to make any css / js changes, you'll need to install the node packages.

`npm install`

`composer update`

`php artisan migrate --package=cartalyst/sentry`

`php artisan:migrate`