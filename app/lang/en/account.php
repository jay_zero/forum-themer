<?php

return array(

	'dashboard.title'			=>	'Account',

	'information.title'			=>	'Update Information',

	'password.title'			=>	'Change Password',

	'downloads.title'			=>	'Downloads',

	'transactions.title'		=>	'Transaction History'

);