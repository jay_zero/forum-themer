<?php

return array(

	'title'				=> 	'Shopping Cart',

	'price'				=>	'Price',
	'product'			=>	'Product',
	'total'				=>	'Total:',

	'action.empty'		=>	'Empty Cart',
	'action.checkout'	=>	'Checkout',

	'items'				=>	'%d item(s) in your cart',

	'error.empty'		=>	'Your shopping cart is currently empty!',
	'error.duplicate'	=>	'This item is already in your cart!',

	'success.add'		=>	'Successfully added <strong>%s</strong> to your cart!',
	'success.update'	=>	'Successfully updated your cart!',
	'success.empty'		=>	'Successfully emptied your cart'
);