<?php

return array(

	'joined'		=>	'Join Date:',
	'posts'			=>	'Post Count:',

	'reputation'	=>	'Reputation:',
	'points'		=>	'Reputation points',

	'activity'		=>	'Latest forum activity'

);