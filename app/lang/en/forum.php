<?php

return array(

	'error.closed'			=>	'This topic has been closed.  If you want to comment about this subject, please create a new topic.',
	'error.no_posts'		=>	'No posts have yet been made here.  But you can always start a new thread.',

	'success.post'			=>	'Forum post deleted successfully',
	'success.delete'		=>	'Forum post deleted successfully',
	'success.edit'			=>	'Edited forum post successfully!',


	'thread.all'			=>	'All Threads',
	'thread.create'			=>	'Create Thread',
	'thread.reply'			=>	'Add Reply',
	'thread.close'			=>	'Close Thread',
	'thread.delete'			=>	'Delete Thread',
	'thread.closed'			=>	'Answered!',

	'thread.meta'			=>	'Started by %s &dash; Last updated %s',

	'post.meta'				=>	'Last updated %s',
	'post.delete'			=>	'Delete',
	'post.edit'				=>	'Edit',

	'index.title'			=>	'Support Forum',

	'like.single'			=>	'1 person likes this',
	'like.plural'			=>	'%s people like this',


	'stats.threads'			=>	'Threads:',
	'stats.posts'			=>	'Replies:'


);