<?php

App::missing(function($exception)
{
    return Response::view('404', array(), 404);
});

App::error(function(Illuminate\Database\Eloquent\ModelNotFoundException $exception) {

    // Log the error
    Log::error($exception);

    // Redirect to error route with any message
    return Response::view('404', array(), 404);
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['before'=>'authenticated'], function(){

	Route::get('logout', ['uses'=>'Themer\Controllers\AuthController@logout', 'as'=>'auth.logout']);

	Route::get('forum/createthread', ['uses'=>'Themer\Controllers\PostController@createThread', 'as'=>'thread.create']);
	Route::post('forum/createthread', ['uses'=>'Themer\Controllers\PostController@storeThread', 'as'=>'thread.create']);

	Route::get('forum/reply/{thread_id}', ['uses'=>'Themer\Controllers\PostController@createPost', 'as'=>'forum.post.create']);
	Route::post('forum/reply/{thread_id}', ['uses'=>'Themer\Controllers\PostController@storePost', 'as'=>'forum.post.create']);


	Route::get('forum/edit/{post_id}', ['uses'=>'Themer\Controllers\PostController@showEdit', 'as'=>'forum.post.edit']);
	Route::post('forum/edit/{post_id}', ['uses'=>'Themer\Controllers\PostController@storeEdit', 'as'=>'forum.post.edit']);
	Route::get('forum/delete/post/{post_id}', ['uses'=>'Themer\Controllers\PostController@deletePost', 'as'=>'forum.post.delete']);

	Route::get('download/{id}', ['uses'=>'Themer\Controllers\DownloadController@download', 'as'=>'download.free']);
	Route::get('download/{id}/{token}', ['uses'=>'Themer\Controllers\DownloadController@download', 'as'=>'download.premium']);

	Route::get('account/dashboard', ['uses'=>'Themer\Controllers\AccountController@dashboard', 'as'=>'account.dashboard']);

	Route::get('account/information', ['uses'=>'Themer\Controllers\AccountController@showInformation', 'as'=>'account.information']);
	Route::post('account/information', ['uses'=>'Themer\Controllers\AccountController@storeInformation', 'as'=>'account.information']);

	Route::get('account/password', ['uses'=>'Themer\Controllers\AccountController@showPassword', 'as'=>'account.password']);
	Route::post('account/password', ['uses'=>'Themer\Controllers\AccountController@storePassword', 'as'=>'account.password']);

	Route::get('account/avatar', ['uses'=>'Themer\Controllers\AccountController@showAvatar', 'as'=>'account.avatar']);
	Route::post('account/avatar', ['uses'=>'Themer\Controllers\AccountController@storeAvatar', 'as'=>'account.avatar']);
	
	Route::get('account/downloads', ['uses'=>'Themer\Controllers\AccountController@downloads', 'as'=>'account.downloads']);
	Route::get('account/transactions', ['uses'=>'Themer\Controllers\AccountController@transactions', 'as'=>'account.transactions']);


	Route::get('checkout/success', ['uses'=>'Themer\Controllers\OrderController@success', 'as'=>'order.success']);


	Route::get('theme/purchase/{id}', ['uses'=>'Themer\Controllers\OrderController@showOrderForm', 'as'=>'theme.purchase']);
	Route::post('theme/purchase/{id}', ['uses'=>'Themer\Controllers\OrderController@processOrder', 'as'=>'theme.purchase']);



	Route::get('like/{id}', ['uses'=>'Themer\Controllers\PostController@like', 'as'=>'like.store.post']);	
	Route::get('theme/favourite/{id}', ['uses'=>'Themer\Controllers\LikeController@storeTheme', 'as'=>'like.store']);	


});


Route::get('notifications', ['uses'=>'Themer\Controllers\NotificationController@index']);
Route::get('notifications/fire', ['uses'=>'Themer\Controllers\NotificationController@fire']);


Route::get('checkout', ['uses'=>'Themer\Controllers\OrderController@showOrderForm', 'as'=>'checkout']);
Route::post('checkout', ['uses'=>'Themer\Controllers\OrderController@processOrder', 'as'=>'checkout']);


Route::get('user/{id}', ['uses'=>'Themer\Controllers\AccountController@showProfile', 'as'=>'account.profile']);

Route::get('login', ['uses'=>'Themer\Controllers\AuthController@login', 'as'=>'auth.login']);
Route::post('login', ['uses'=>'Themer\Controllers\AuthController@postLogin']);


Route::get('activate/{user_id}/{code}', ['uses'=>'Themer\Controllers\AuthController@getActivate', 'as'=>'auth.activate']);

Route::get('register', ['uses'=>'Themer\Controllers\AuthController@getRegister', 'as'=>'auth.register']);
Route::post('register', ['uses'=>'Themer\Controllers\AuthController@postRegister']);


Route::get('forum', ['uses'=>'Themer\Controllers\PostController@index', 'as'=>'forum.index']);
Route::get('forum/thread/{thread_id}', ['uses'=>'Themer\Controllers\PostController@thread', 'as'=>'forum.thread']);


Route::get('forum/close/{thread_id}', ['before' => 'moderate','uses'=>'Themer\Controllers\PostController@closeThread', 'as'=>'forum.thread.close']);
Route::get('forum/delete/thread/{thread_id}', ['before' => 'moderate','uses'=>'Themer\Controllers\PostController@deleteThread', 'as'=>'forum.thread.delete']);


Route::get('cart/add/{id}', ['uses' => 'Themer\Controllers\CartController@add', 'as' => 'cart.add'] );
Route::get('cart/remove/{id}', ['uses' => 'Themer\Controllers\CartController@remove', 'as' => 'cart.remove'] );
Route::get('cart', ['uses' => 'Themer\Controllers\CartController@cart', 'as' => 'cart.index'] );
Route::get('cart/empty', ['uses' => 'Themer\Controllers\CartController@cart_empty', 'as' => 'cart.empty'] );
Route::get('cart/copyright/{id}', ['uses' => 'Themer\Controllers\CartController@copyrightService', 'as' => 'cart.copyright'] );


Route::get('{type}-{tag}-themes/{slug}', [ 'uses'=>'Themer\Controllers\ProjectController@project', 'as'=>'project.single']);


Route::get('themes/free', [ 'uses'=>'Themer\Controllers\ProjectController@projectsFree', 'as'=>'projects.free']);
Route::get('free-{slug}-themes', [ 'uses'=>'Themer\Controllers\ProjectController@projectsFree', 'as'=>'projects.free.type']);

Route::get('themes/premium', [ 'uses'=>'Themer\Controllers\ProjectController@projectsPremium', 'as'=>'projects.premium']);
Route::get('premium-{slug}-themes', [ 'uses'=>'Themer\Controllers\ProjectController@projectsPremium', 'as'=>'projects.premium.type']);


Route::get('/', [ 'uses'=>'Themer\Controllers\PagesController@showHome', 'as'=>'home']);
Route::get('/privacy', [ 'uses'=>'Themer\Controllers\PagesController@showPrivacy', 'as'=>'privacy']);
Route::get('/contact', [ 'uses'=>'Themer\Controllers\PagesController@showContact', 'as'=>'contact']);






Route::group(['before'=>'authenticated', 'prefix' => 'admin'], function(){

	Route::get('/', [ 'uses'=>'Themer\Controllers\AdminController@showHome', 'as'=>'admin.dashboard']);

	Route::get('project/add', [ 'uses'=>'Themer\Controllers\ProjectController@adminAdd', 'as'=>'admin.project.add']);
	Route::post('project/add', [ 'uses'=>'Themer\Controllers\ProjectController@storeProject', 'as'=>'admin.project.add']);

});