<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderLinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_lines', function($table)
	    {
	    	$table->increments('id')->unsigned();
	    	$table->string('item_name');
	    	$table->string('item_desc');
	    	$table->integer('item_cost');
	    	$table->integer('project_id');
	    	$table->integer('copyright');
	    	$table->integer('copyright_cost');
	    	$table->softDeletes();
	    	$table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_lines');
	}

}
