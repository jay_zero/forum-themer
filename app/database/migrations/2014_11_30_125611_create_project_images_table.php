<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_images', function($table)
	    {
	    	$table->increments('id')->unsigned();
	    	$table->integer('project_id');
	    	$table->string('filename');
	    	$table->integer('order')->default(0);
	    	$table->softDeletes();
	    	$table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_images');
	}
}
