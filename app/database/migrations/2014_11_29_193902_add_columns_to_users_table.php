<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			// $table->tinyInteger('stripe_active')->default(0);
			$table->integer('post_count')->default(0);
			$table->string('username');
			$table->string('avatar')->nullable();
			// $table->string('stripe_subscription')->nullable();
			// $table->string('stripe_plan', 25)->nullable();
			// $table->string('last_four', 4)->nullable();
			// $table->timestamp('trial_ends_at')->nullable();
			// $table->timestamp('subscription_ends_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn(
				"post_count", "username", "avatar"
			);
		});
	}

}
