<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadTokenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('downloads', function($table)
	    {
	    	$table->increments('id')->unsigned();
	    	$table->string('token');
	    	$table->integer('user_id');
	    	$table->integer('project_id');
	    	$table->integer('downloads');
	    	$table->timestamp('expires_at'); // May not use this for the time being
	    	$table->softDeletes();
	    	$table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('downloads');
	}

}
