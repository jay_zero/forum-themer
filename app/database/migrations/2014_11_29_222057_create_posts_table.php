<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function($table)
	    {
	    	$table->increments('id')->unsigned();
	    	$table->string('title');
	    	$table->integer('parent')->default(0);
	    	$table->integer('user_id');
	    	$table->integer('closed')->default(0);
	    	$table->text('body');
	    	$table->dateTime('last_post_date');
	    	$table->softDeletes();
	    	$table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}


}
