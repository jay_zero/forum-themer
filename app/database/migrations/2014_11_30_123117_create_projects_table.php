<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function($table)
	    {
	    	$table->increments('id')->unsigned();
	    	$table->integer('price');
	    	$table->integer('likes')->default(0);
	    	$table->integer('views')->default(0);
	    	$table->string('title');
	    	$table->integer('software_id');
	    	$table->string('slug')->unique();
	    	$table->integer('downloads')->default(0);
	    	$table->text('body');
	    	$table->softDeletes();
	    	$table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}
}
