<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
        $this->call('UserTableSeeder');
        $this->call('SoftwareTableSeeder');
        $this->call('ProjectTableSeeder');
        $this->call('ForumTagTableSeeder');
        $this->call('PostTableSeeder');
	}
}
