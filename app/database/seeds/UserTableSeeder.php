<?php

class UserTableSeeder extends Seeder {
    public function run()
    {
        DB::table('users')->truncate();
        $date = date('Y-m-d H:i:s');
        $users = [
            [
                'email'         => 'rob.milward@gmail.com',
                'password'      => 'password',
                'first_name'    => 'Rob',
                'last_name'     => 'Milward',
                'username'      => 'Rawbz',
                'activated'     => 1,
                'activated_at'  => $date,
                'email_updates' => 1
            ],
            [
                'email'         => 'test@example.com',
                'password'      => 'password',
                'first_name'    => 'Test',
                'last_name'     => 'Example',
                'username'      => 'MrT',
                'activated'     => 1,
                'activated_at'  => $date,
                'email_updates' => 1
            ],
            [
                'email'         => 'jamiewatson07@gmail.com',
                'password'      => 'password',
                'first_name'    => 'Jamie',
                'last_name'     => 'Watson',
                'username'      => 'Jamie',
                'activated'     => 1,
                'activated_at'  => $date,
                'email_updates' => 1
            ]
        ];

        foreach($users as $data) {
            $user = new \Themer\Models\User();
            $user->setHasher(new \Cartalyst\Sentry\Hashing\NativeHasher);
            $user->fill($data);
            $user->save();
        }
    }
} 