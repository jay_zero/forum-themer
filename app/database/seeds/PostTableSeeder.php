<?php

class PostTableSeeder extends Seeder {
    public function run()
    {
        DB::table('posts')->truncate();

        $faker = Faker\Factory::create();
        $tags = \Themer\Models\Tag::lists('id');
        $number_threads = $faker->numberBetween(9, 15);

        for ($i = 1; $i <= $number_threads; $i++) {
            $last_id = 0;
            $number_posts = $faker->numberBetween(1,10);
            for ($j = 1; $j <= $number_posts; $j++) {
                $data = [
                    'title'     => $faker->sentence(8),
                    'user_id'   => \Themer\Models\User::orderByRaw("RAND()")->first()->id,
                    'body'      => $faker->sentence(20),
                    'parent'    => $last_id
                ];
                $post = \Themer\Models\Post::create($data);

                $last_id = $last_id === 0 ? $post->id : $last_id;
                shuffle($tags);
                $tag_ids = array_slice($tags, 0, $faker->numberBetween(0, count($tags)));
                $post->tags()->sync($tag_ids);
            }
        }
    }
} 