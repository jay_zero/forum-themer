<?php

class SoftwareTableSeeder extends Seeder {
    public function run()
    {
        DB::table('software')->truncate();

        $software = [
            ['title' => 'MyBB', 'slug'  => 'mybb'],
            ['title' => 'IPBoard', 'slug'  => 'ipboard'],
            ['title' => 'Zetaboards', 'slug'  => 'zetaboards'],
        ];

        foreach($software as $data) {
            \Themer\Models\Software::create($data);
        }
    }
} 