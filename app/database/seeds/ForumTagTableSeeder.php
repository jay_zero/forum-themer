<?php

class ForumTagTableSeeder extends Seeder {
    public function run()
    {
        DB::table('forum_tags')->truncate();

        $faker = Faker\Factory::create();

        for($i = 1; $i < $faker->numberBetween(5, 10); $i++) {
            $data = [
                'name' => 'Tag ' . $i,
                'slug' => 'tag' . $i,
                'order' => $i
            ];
            \Themer\Models\Tag::create($data);
        }
    }
}