<?php

class ProjectTableSeeder extends Seeder {
    public function run()
    {
        DB::table('projects')->truncate();

        $faker = Faker\Factory::create();
        $faker->seed(1234);
        foreach(\Themer\Models\Software::all() as $software) {
            $number_projects = $faker->numberBetween(2, 10);
            for ($i = 1; $i <= $number_projects; $i++) {
                $data = [
                    'price'         => $faker->numberBetween(0, 1) ? 0 : $faker->numberBetween(10, 50) * 100,
                    'title'         => $software->title . ' Theme ' . $i,
                    'software_id'   => $software->id,
                    'slug'          => strtolower($software->slug . '-' . $i),
                    'downloads'     => $faker->numberBetween(0, 100),
                    'body'          => $faker->sentence(80),
                    'short_desc'    => $faker->sentence(7),
                    'small_image'   => 'placeholder-' . $faker->numberBetween(1, 5) . '.png'
                ];
                \Themer\Models\Project::create($data);
            }
        }
    }
} 