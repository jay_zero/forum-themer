@extends('layouts.full')

@section('title', 'Order Successful')

@section('content')
	<h2>Thanks for your custom!</h2>
	<p>Thanks for placing an order with ForumThemer!  Your order was a success and we hope you'll be happy with your purchases.  Your download links are available below, or you can download any time from <a href="#">your downloads area</a> in your user control panel.</p>

	<h5>Order Number: {{$order->id}}</h5>

	<h2>Order Summary:</h2>

	<ul class="downloads">
		@foreach( $order->lines as $line )
			<li class="download">
				<div class="download--image">
					<img src="http://placehold.it/100x100" alt="{{$line->item_name}}" />
				</div>

				<div class="download--information">
					<h3>{{link_to_route( 'project.single', $line->item_name, ['premium', $line->project->software->slug, $line->project->slug])}}</h3>
					@if( $line->copyright == 1 )
						<p>You purchased Copyright Removal for this theme, please feel free to hide the copyright line on your website</p>
					@endif
				</div>

				<div class="download--options">
					{{icon_button_route( 'download.premium', 'Download', [$line->download->project_id, $line->download->token], 'fa-download', 'primary')}}
				</div>
			</li>
		@endforeach
		</ul>
@stop
	