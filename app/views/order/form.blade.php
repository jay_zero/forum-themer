@extends('layouts.full')


@section('content')

	<div class="container">

		<h1>Place Order</h1>

		<p>You are about to complete a purchase for <strong>{{format_currency(Cart::total())}}</strong></p>

		<form action="" method="POST" id="payment-form">


		@if( !Sentry::check() )
			<h2>Your Profile</h2>

			<p>It looks like you don't have an account with us yet!  Fill in your information below and we'll create your account so you can keep track of your downloads and orders.</p>
			
			{{ Form::label('username', 'Username') }}
            {{ Form::text('username', null, ['placeholder' => 'Username']) }}
            {{ errors_for('username', $errors) }}

            {{ Form::label('first_name', 'First Name') }}
            {{ Form::text('first_name', null, ['placeholder' => 'First Name']) }}
            {{ errors_for('first_name', $errors) }}

            {{ Form::label('last_name', 'Last Name') }}
            {{ Form::text('last_name', null, ['placeholder' => 'Last Name']) }}
            {{ errors_for('last_name', $errors) }}

            {{ Form::label('email', 'Email') }}
            {{ Form::text('email', null, ['placeholder' => 'Email']) }}
            {{ errors_for('email', $errors) }}

            {{ Form::label('password', 'Password') }}
            {{ Form::password('password') }}
            {{ errors_for('password', $errors) }}

            {{ Form::label('password_confirmation', 'Confirm Password') }}
            {{ Form::password('password_confirmation') }}
		@endif

		<h2>Payment Information</h2>

		  <span class="payment-errors"></span>

		  <div class="form-row">
		    <label>
		      <span>Card Number</span>
		      <input type="text" size="20" data-stripe="number"/>
		    </label>
		  </div>

		  <div class="form-row">
		    <label>
		      <span>CVC</span>
		      <input type="text" size="4" data-stripe="cvc"/>
		    </label>
		  </div>

		  <div class="form-row">
		    <label>
		      <span>Expiration (MM/YYYY)</span>
		      <input type="text" size="2" data-stripe="exp-month"/>
		    </label>
		    <span> / </span>
		    <input type="text" size="4" data-stripe="exp-year"/>
		  </div>

		  @include('global.notice', ['message'=>'Don\'t worry. This form is 100% secure. Your private credit card data will never touch our servers.', 'class'=>'success'])
		  <button type="submit">Submit Payment</button>
		</form>

	</div>

@stop

@section('javascript')

	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript" src="{{URL::asset('assets/js/billing.min.js')}}"></script>

@stop