@extends('layouts.sidebar')

@section('title', $project->title)

@section('top')
	@if( count($project->images) > 0 )
	<div class="project-slider">
    <div class="container">
      <button type="button" class="slider-prev"><i class="fa fa-chevron-left"></i></button>
      <button type="button" class="slider-next"><i class="fa fa-chevron-right"></i></button>
      
        <div class="project-images hide-for-small">
            @foreach( $project->images as $image )
            	<div><img src="{{URL::asset('assets/images/themes/'.$image->filename)}}" alt="" /></div>
            @endforeach
        </div>
      </div>
    </div>
    @endif
@stop

@section('sidebar')

      <div class="widget stats">
        <div class="stat-row">
          <label>Software</label>
          <a href="{{ URL::route('projects.' . ($project->price<1?'free':'premium') . '.type', $project->software->slug) }}">{{$project->software->title}}</a>
        </div>
        <div class="stat-row">
          <label>Downloads</label>
          {{$project->downloads}}
        </div>
        <div class="stat-row">
          <label>Views</label>
          {{$project->views}}
        </div>
        <div class="stat-row">
          <label>Likes</label>
          {{$project->likes}}
        </div>
      </div>
      <div class="widget actions">
      	@if( Sentry::check() )
          @if( !$project->liked( Sentry::getUser()->id ))
        	 <a href="/theme/favourite/{{$project->id}}" class="button secondary" id="addToFavourites">Favourite</a>
          @else
            <a href="/theme/favourite/{{$project->id}}" class="button secondary" id="addToFavourites">Remove From Favourites</a>
          @endif
        @endif
        @if( $download_link )
          <a href="{{$download_link}}" class="button primary">Download</a>
        @else
        <a href="{{URL::route('cart.add', $project->id)}}" class="button primary">Add to Cart <span>{{format_currency($project->price)}}</span></a>
          <!--{{link_to_route('cart.add', 'Add to Cart', $project->id, ['class'=>'button primary'])}}-->
        @endif
      </div>

@stop

@section('content')

  <ul class="accordion-tabs-minimal">
  <li class="tab-header-and-content">
    <a href="#" class="tab-link is-active">Description</a>
    <div class="tab-content">
      {{$project->present()->description}}
    </div>
  </li>
  <li class="tab-header-and-content">
    <a href="#" class="tab-link">Changelog</a>
    <div class="tab-content">
      <p>Ut laoreet augue et neque pretium non sagittis nibh pulvinar. Etiam ornare tincidunt orci quis ultrices. Pellentesque ac sapien ac purus gravida ullamcorper. Duis rhoncus sodales lacus, vitae adipiscing tellus pharetra sed. Praesent bibendum lacus quis metus condimentum ac accumsan orci vulputate. Aenean fringilla massa vitae metus facilisis congue. Morbi placerat eros ac sapien semper pulvinar. Vestibulum facilisis, ligula a molestie venenatis, metus justo ullamcorper ipsum, congue aliquet dolor tortor eu neque. Sed imperdiet, nibh ut vestibulum tempor, nibh dui volutpat lacus, vel gravida magna justo sit amet quam. Quisque tincidunt ligula at nisl imperdiet sagittis. Morbi rutrum tempor arcu, non ultrices sem semper a. Aliquam quis sem mi.</p>
    </div>
  </li>
</ul>
@stop