@extends('layouts.sidebar')

@section('title')
	{{ucwords($type)}} {{ ($current ? $current->title: 'Forum') }} Themes
@stop

@section('sidebar')
	<div class="sidebar--widget">
		<h4>Software</h4>
		<ul class="links">
			@foreach( $tags as $tag )
				<li>{{link_to_route('projects.'.$type.'.type', $tag->title, ['tag'=>$tag->slug], ['class'=> ( $current && $current->slug == $tag->slug ? 'active': '') ])}}</li>
			@endforeach
		</ul>
	</div>
@stop

@section('content')

	@if( $projects->count() > 0 )

		<div class="project-grid">
			@foreach( $projects as $project )
				@include('partials.project', ['project'=>$project, 'type' => $type])
	        @endforeach
        </div>


        <div class="page-links">
        	<div class="pages">{{ sprintf( Lang::get('project.total_pages'), $projects->count(), $projects->getTotal(), $projects->getLastPage() )}}</div>
			<!-- <ul>
				{{ with( new Themer\Presenters\PaginatePresenter( $projects ) )->render() }}
			</ul> -->
			{{$projects->links()}}
		</div>
    @else
    	@include('forum.notice', ['message'=>'We\'ve not yet added any themes for this software.  Check back soon.'])
    @endif

    @if( $type == 'free' && false )
    	@if( $premium->count() > 0 )
		@if( $current )
			<h2>Premium {{$current->title}} themes</h2>
		@else
			<h2>Premium Themes</h2>
		@endif
		<div class="project-grid">
			@foreach( $premium as $project )
				@include('partials.project', ['project'=>$project, 'type' => 'premium'])
	        @endforeach
        </div>
        @endif
    @endif

@stop

@section('footer')
	<div class="feature">
		<i class="fa fa-life-ring"></i>
		Free theme support included
	</div>
	<div class="feature">
		<i class="fa fa-paint-brush"></i>
		Use your theme on multiple forums
	</div>
	<div class="feature">
		<i class="fa fa-bolt"></i>
		Instant download access
	</div>
@stop