@extends('layouts.full')

@section('content')
	{{Form::open()}}

	{{ Form::label('name', 'Name') }}
    {{ Form::text('name', null, ['placeholder' => 'Name']) }}
    {{ errors_for('name', $errors) }}

    {{ Form::label('email', 'Email') }}
    {{ Form::text('email', null, ['placeholder' => 'Email']) }}
    {{ errors_for('email', $errors) }}

    {{ Form::label('enquiry', 'Enquiry') }}
    {{ Form::textarea('enquiry', null) }}
    {{ errors_for('enquiry', $errors) }}

	{{Form::captcha(['theme'=>'custom', 'custom_theme_widget' => 'recaptcha_widget'])}}

	<div id="recaptcha_widget" style="display:none">

        <div class="control-group">
            <label class="control-label">reCAPTCHA</label>
            <div class="controls">
                <a id="recaptcha_image" href="#" class="thumbnail"></a>
                <div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect please try again</div>
            </div>
        </div>

           <div class="control-group">
               <label class="recaptcha_only_if_image control-label">Enter the words above:</label>
              <label class="recaptcha_only_if_audio control-label">Enter the numbers you hear:</label>

              <div class="controls">
                  <div class="input-append">
                      <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" class="input-recaptcha" />
                      <a class="button primary" href="javascript:Recaptcha.reload()"><i class="fa fa-refresh"></i></a>
                      <a class="button primary recaptcha_only_if_image" href="javascript:Recaptcha.switch_type('audio')"><i title="Get an audio CAPTCHA" class="fa fa-headphones"></i></a>
                      <a class="button primary recaptcha_only_if_audio" href="javascript:Recaptcha.switch_type('image')"><i title="Get an image CAPTCHA" class="fa fa-image"></i></a>
                    <a class="button primary" href="javascript:Recaptcha.showhelp()"><i class="fa fa-question"></i></a>
                  </div>
              </div>
        </div>

    </div>

    {{Form::submit('Send Enquiry')}}

	{{Form::close()}}
@stop