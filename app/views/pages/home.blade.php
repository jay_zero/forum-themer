@extends('layouts.full')

@section('title', trans('pages.home.title') )

@section('content')

<div class="container">
    <!-- <div class="intro-text">
        <h1>Elegantly designed forum themes</h1>
        <h3>Hand crafted with <span>Love</span> and <span>Awesomeness</span></h3>
    </div> -->

    <img src="http://placehold.it/1200x500" />

    <div class="features-wrap">

        <div class="features-wrap--feature">
            <div class="icon">
                <i class="fa fa-life-ring"></i>
            </div>
            <div class="features-wrap--feature_main">
                <h3>Free support included</h3>
                <p>Use our online support forums to get support with your purchase any time.</p>
            </div>
        </div>

        <div class="features-wrap--feature">
            <div class="icon">
                <i class="fa fa-paint-brush"></i>
            </div>
            <div class="features-wrap--feature_main">
                <h3>Multi use themes</h3>
                <p>Use your purchase on as many different forums as you'd like.</p>
            </div>
        </div>

        <div class="features-wrap--feature">
            <div class="icon">
                <i class="fa fa-bolt"></i>
            </div>
            <div class="features-wrap--feature_main">
                <h3>Instant Access</h3>
                <p>All your purchases are made instantly available in your account area.</p>
            </div>
        </div>

    </div>

    <div class="block-title">
        <h2>Premium Themes</h2>
    </div>

    <ul class="accordion-tabs-minimal">
      <li class="tab-header-and-content">
        <a href="#" class="tab-link is-active">Newest</a>
        <div class="tab-content">
            <div class="project-grid-4up">
                @foreach( $latestPremium as $project )
                    @include('partials.project', ['project'=>$project, 'type' => 'premium'])
                @endforeach
            </div>
        </div>
      </li>
      <li class="tab-header-and-content">
        <a href="#" class="tab-link">Most Liked</a>
        <div class="tab-content">
           <div class="project-grid-4up">
                @foreach( $popular as $project )
                    @include('partials.project', ['project'=>$project, 'type' => 'premium'])
                @endforeach
            </div>
        </div>
      </li>
      <li class="tab-header-and-content">
        <a href="#" class="tab-link">Most Downloaded</a>
        <div class="tab-content">
            <div class="project-grid-4up">
                @foreach( $downloaded as $project )
                    @include('partials.project', ['project'=>$project, 'type' => 'premium'])
                @endforeach
            </div>
        </div>
      </li>
    </ul>

    <div class="block-title">
        <h2>Newest Free Themes</h2>
    </div>

    <div class="project-grid-4up">
        @foreach( $free as $project )
            @include('partials.project', ['project'=>$project, 'type' => 'free'])
        @endforeach
    </div>

</div>

@stop