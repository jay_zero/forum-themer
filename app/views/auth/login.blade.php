@extends('layouts.full')

@section('title', trans('auth.login.title') )

@section('content')
    <div class="auth">

        <div class="auth--block">
            <p><span>Don't have an account?</span>  Registration is free and only takes a few seconds.  With an account you will be able to respond in our support forum as well as make quick purchases to new themes.</p>

            {{link_to_route('auth.register', 'Register for free', null, ['class'=>'button primary'])}}
        </div>

        <div class="auth--block">
            {{Form::open()}}
                {{ Form::label('username', 'Username') }}
                {{ Form::text('username', null, ['placeholder' => 'Email']) }}
                {{ errors_for('username', $errors) }}

                {{ Form::label('password', 'Password') }}
                {{ Form::password('password') }}
                {{ errors_for('password', $errors) }}

                <label><input type="checkbox"> Remember Me</label>

                <input type="submit" class="button" value="Login" />

                <a href="javascript:void(0)" class="forgot">Forgot password?</a>
            {{Form::close()}}
        </div>
    </div>
@stop