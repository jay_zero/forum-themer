@extends('layouts.full')

@section('title', trans('auth.register.title') )

@section('content')

     <div class="register-form">

        <div class="register">
          {{ Form::open() }}
            {{ Form::label('username', 'Username') }}
            {{ Form::text('username', null, ['placeholder' => 'Username']) }}
            {{ errors_for('username', $errors) }}

            {{ Form::label('first_name', 'First Name') }}
            {{ Form::text('first_name', null, ['placeholder' => 'First Name']) }}
            {{ errors_for('first_name', $errors) }}

            {{ Form::label('last_name', 'Last Name') }}
            {{ Form::text('last_name', null, ['placeholder' => 'Last Name']) }}
            {{ errors_for('last_name', $errors) }}

            {{ Form::label('email', 'Email') }}
            {{ Form::text('email', null, ['placeholder' => 'Email']) }}
            {{ errors_for('email', $errors) }}

            {{ Form::label('password', 'Password') }}
            {{ Form::password('password') }}
            {{ errors_for('password', $errors) }}

            {{ Form::label('password_confirmation', 'Confirm Password') }}
            {{ Form::password('password_confirmation') }}

            {{ Form::submit('Sign Up', ['class'=>'button']) }}

          {{ Form::close() }}
        </div>
    </div>
    
@stop