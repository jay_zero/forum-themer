@extends('layouts.master')

@section('layout')

    @yield('top')

    <div class="container">
        <aside class="sidebar">
            @yield('sidebar')
        </aside>

        <section class="primary-content">
            @yield('content')
        </section>
    </div>

@stop