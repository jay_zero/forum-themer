<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>@yield('title') | Forum Themer</title>

    <link href="/assets/css/app.css" rel="stylesheet">

    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Titillium+Web:400,400italic,600italic,600,700,700italic,900,300italic,300,200italic,200" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Oxygen:400,700,900" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    @yield('css')

    @if( isset($ogdata) )
        <meta property="og:title" content="{{$og->title}}">
        <meta property="og:type" content="{{$og->type}}">
        <meta property="og:url" content="{{$og->url}}">
        <meta property="og:image" content="{{$og->image}}">
        <meta property="og:site_name" content="{{$og->sitename}}">
        <meta property="og:description" content="{{$og->desc}}">
    @endif

    <meta name="stripe-key" content="@stripeKey">

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>


    <div class="top-bar">
        <div class="container">
            @include('partials.topbar')
        </div>
    </div>

    <header class="header">

        <div class="container">
            <div class="header--logo">
                <a href="{{URL::route('home')}}"><img src="{{URL::asset('assets/images/temp-logo.png')}}" /></a>
            </div>

            <div class="header--cart">
                 <div class="mini-cart">
                    <a href="{{ URL::route('cart.index') }}" class="button secondary"><i class="fa fa-shopping-cart"></i> {{Cart::count()}} Item(s) in your cart</a>
                </div>
            </div>
        </div>

    </header>

    <nav class="navigation">
        <div class="container">
            @include('partials.navigation')
        </div>
    </nav>

    @if( Session::has('success_message') or Session::has('error_message') )
        <div class="container">
            @if( Session::has('success_message') )
                @include( 'global.notice', ['message'=>Session::get('success_message'), 'class'=>'success'])
            @endif

            @if( Session::has('error_message') )
                @include( 'global.notice', ['message'=>Session::get('error_message'), 'class'=>'error'])
            @endif
        </div>
    @endif

    @yield('layout')

    @include('partials.footer')

    @section('scripts')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="{{URL::asset('assets/js/app.min.js')}}"></script>
        <script async src="https://assets.helpful.io/assets/widget.js"></script>
        @if( App::isLocal() )
            <script src="http://localhost:1337/livereload.js"></script>
        @endif
    @show

    @yield('javascript')

  </body>
</html>