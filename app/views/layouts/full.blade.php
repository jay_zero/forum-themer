@extends('layouts.master')

@section('layout')

	<div class="container">
		@yield('content')
	</div>

@stop