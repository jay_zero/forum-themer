@extends('layouts.full')

@section('content')

	<div class="container">
		{{ Form::open() }}

			{{ Form::label('title', 'Title') }}
			{{ Form::text('title', null, ['placeholder' => 'Title']) }}
			{{ errors_for('title', $errors) }}

			{{ Form::label('tags[]', 'Tags') }}
			{{ Form::select('tags[]', $tags, null, array('multiple')) }}
			{{ errors_for('tags', $errors) }}

			{{ Form::label('body', 'Body') }}
			<small>Use of BB Code is allowed</small>
			{{ Form::textarea('body') }}
			{{ errors_for('body', $errors) }}

			{{ Form::submit('Create Thread') }}

		{{ Form::close() }}
	</div>
@stop