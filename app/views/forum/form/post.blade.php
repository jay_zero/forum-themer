{{ Form::open(['route'=>array('forum.post.create', $postid)]) }}

	<small>Use of BB Code is allowed</small>
	{{ Form::textarea('body') }}
	{{ errors_for('body', $errors) }}

	{{ Form::submit('Add Reply') }}

{{ Form::close() }}