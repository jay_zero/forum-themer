@extends('layouts.full')

@section('content')

	<div class="container">
		{{ Form::model($post) }}

			{{ Form::label('body', 'Body') }}
			<small>Use of BB Code is allowed</small>
			{{ Form::textarea('body') }}
			{{ errors_for('body', $errors) }}

			{{ Form::submit('Edit Post') }}

		{{ Form::close() }}
	</div>
@stop