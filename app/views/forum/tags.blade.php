<h4>Categories</h4>
<ul class="links">
	<li>{{link_to_route('forum.index', trans('forum.thread.all'), null, ['class'=> (Input::get('tag') ?: 'active') ])}}</li>
	@foreach( $tags as $tag )
		<li>{{link_to_route('forum.index', $tag->name, ['tag'=>$tag->slug], ['class'=> (Input::get('tag') !== $tag->slug ?: 'active') ])}}</li>
	@endforeach
</ul>