@extends('layouts.sidebar')

@section('title', $post->title )

@section('sidebar')
	<div class="sidebar--widget">
		@include('forum.tags', ['tags'=>$tags])
	</div>
@stop

@section('content')

	<section class="thread">

  <h1>{{$post->title}}</h1>



		@if( Input::get('page') == 1 || !Input::has('page') )
		  @include('forum.postbit', ['reply'=>$post])
    @endif

		@foreach( $post->replies as $reply )
		  @include('forum.postbit', ['reply'=>$reply])
    @endforeach
        
        @if( Sentry::check() )
          @if( !$post->closed || Sentry::getUser()->hasAnyAccess(['admin', 'moderate']) )
            @include('forum.form.post', ['postid' => $post->id])
          @endif
          <div class="post-buttons">
            @if(Sentry::getUser()->hasAnyAccess(['admin', 'moderate']))
              {{ link_to_route('forum.thread.delete', trans('forum.thread.delete'), $post->id, ['class'=>'button secondary']) }}
              {{ link_to_route('forum.thread.close', trans('forum.thread.close'), $post->id, ['class'=>'button secondary']) }}
            @endif
          </div>
        @else
          <div class="message">
            {{link_to_route('auth.login', 'Log in')}} or {{link_to_route('auth.register', 'create an account')}} to take part in this discussion.
          </div>
        @endif

        @if( $post->replies->count() > 0 )
          <div class="pagination">
            {{$post->replies->links()}}
          </div>
        @endif

	</section>

@stop