@extends('layouts.sidebar')

@section('title')
	@if( !Input::get('tag') )
		{{ trans('forum.index.title') }}
	@endif
@stop

@section('sidebar')
	<div class="sidebar--widget">
		@include('forum.tags', ['tags'=>$tags])
	</div>

	<div class="sidebar--widget">
		<h4>Forum Statistics</h4>
		<div class="sidebar--widget_content">
			<p>{{ Lang::get('forum.stats.threads') }} {{ $stats->total_threads }}</p>
			<p>{{ Lang::get('forum.stats.posts') }} {{ $stats->total_replies }}</p>
		</div>
	</div>
@stop

@section('content')

	<div class="forums">

	@if( $posts->count() > 0 )
		@foreach( $posts as $post )
			<article class="thread">
				{{$post->user->present()->avatar_image}}
				<div class="info">
				  <h3>{{link_to_route('forum.thread', $post->title, $post->id)}}</h3>
				  <p class="meta">{{$post->present()->meta}}</p>
				</div>

				<div class="replies">
					@if( $post->closed )
				  		<a href="#" class="solved"><i class="fa fa-check"></i> {{trans('forum.thread.closed')}}</a>
				  	@endif
				  <a href="#">{{$post->replies->count()}}</a>
				</div>
			</article>
        @endforeach

        @if( Sentry::check() )
          <div class="post-buttons">
            {{ link_to_route('thread.create', trans('forum.thread.create'), null, ['class'=>'button primary'] ) }}
          </div>
        @endif

        <div class="pagination">
          <ul>
            {{ with( new Themer\Presenters\PaginatePresenter( $posts ) )->render() }}
          </ul>
        </div>
    @else
    	@include('forum.notice', ['message'=> trans('forum.error.no_posts')])
    @endif

	</div>

@stop