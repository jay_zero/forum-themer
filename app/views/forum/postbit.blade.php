<div class="thread--post">
    <div class="thread--post_user">
      {{$reply->user->present()->avatar_image}}
      <div class="thread--post_user-info">
        <h3>{{$reply->user->present()->user_link}} <span class="rep">- {{ $reply->user->reputation or '0' }} {{ Lang::get('profile.points') }}</span></h3>
        <p class="meta">{{$reply->present()->post_meta}}</p>
      </div>
    </div>

    <div class="thread--post_body">
      {{$reply->present()->bb_body}}
    </div>

    <span class="likes">
    @if( $reply->likes == 1 )
    {{sprintf( Lang::get('forum.like.single'), $reply->likes )}}
    @elseif( $reply->likes > 1 )
    {{sprintf( Lang::get('forum.like.plural'), $reply->likes )}}
    @endif
    </span>

    @if( Sentry::check() )
      <div class="mod-opts">
        @if(Sentry::getUser()->hasAnyAccess(['admin', 'moderate']) || Sentry::getUser()->id == $reply->user->id )
          {{link_to_route('forum.post.edit', trans('forum.post.edit'), $reply->id)}}
          {{link_to_route('forum.post.delete', trans('forum.post.delete'), $reply->id)}}
        @endif
        {{link_to_route('like.store.post', $reply->liked( Sentry::getUser()->id ) ? 'Unlike' : 'Like', $reply->id, ['class'=>'like'])}}
      </div>
    @endif
</div>