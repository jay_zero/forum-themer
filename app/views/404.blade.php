@extends("layouts.master")

@section('title', '404')

@section("layout")
	<div class="container">
		<h1>Oops!  We couldn't find that page!</h1>

		<p>It seems we couldn't find that page on our website.  Maybe the URL bringing you here is out-dated or never existed in the first place.  Check the URL and try again.  Alternatively you can use the search below to find the page you're looking for.</p>

		<script type="text/javascript">
		  var GOOG_FIXURL_LANG = 'en';
		  var GOOG_FIXURL_SITE = 'http://www.forumthemer.net'
		</script>
		<script type="text/javascript"
		  src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js">
		</script>
	</div>
@stop