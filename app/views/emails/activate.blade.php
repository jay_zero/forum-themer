@extends('emails.master')

@section('title')
	Account Activation Required
@stop

@section('content')
	<h1>Account Activation Required</h1>
	Hey {{ $username }},<br>

	You're one step away from becoming a full member over at <a href="http://www.forumthemer.net">Forum Themer</a>.  Simply click on the link below to activate your account.<br /><br />
	{{link_to_route('auth.activate', 'Activate your account', array( $user_id, $activation ) )}}
@stop