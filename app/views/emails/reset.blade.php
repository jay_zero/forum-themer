@extends('emails.master')

@section('title')
	Password Reset Code
@stop

@section('content')
	<h1>Password Reset Code</h1>
	Hey {{ $username }},<br>

	You have requested to reset your password at <a href="http://www.forumthemer.net">Forum Themer</a>.  To continue follow the link below. <br>

	{{link_to_route('account.reset', 'Change your password', [ $code ] )}}<br>

	Thanks,<br >
	Forum Themer Team
@stop