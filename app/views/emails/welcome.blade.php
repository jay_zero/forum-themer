@extends('emails.master')

@section('title')
	Weclome to Forum Themer
@stop

@section('content')
	<h1>Welcome to Forum Themer</h1>
	Hey {{ $username }},<br><br>

	Welcome to Forum Themer, where you can find lots of ready made themes to make your forum stand out from the crowd.  Get started now by browsing our <a href="#">Free</a> and <a href="#">Premium Themes</a>.  Of course, if you need help with anything, head on over to our {{ link_to_route('forum.index', 'support forum') }} and we'll do the best we can to help you get started!<br /><br />

	We hope you enjoy your time with us!<br />
	Best Regards,<br />
	Jamie
	
@stop