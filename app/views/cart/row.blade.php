<tr>
	<td width="5%"><img src="http://placehold.it/100x100" /></td>
	<td>
		<h4>{{$item->name}} ({{link_to_route('cart.remove', 'Remove', $item->rowid)}})</h4>
		<label><input type="checkbox" class="copy" value="{{$item->rowid}}" {{$item->options->has('copyright')?'checked':''}}/> Purchase copyright removal for this theme? ( <span>+{{format_currency(Config::get('app.copyright_cost'))}}</span> )</label>
	</td>
	<td align="right">{{format_currency($item->price)}}</td>
</tr>