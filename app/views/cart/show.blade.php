@extends('layouts.full')

@section('title', trans('cart.title'))

@section('content')
	@if( Cart::count() > 0 )
		<p class="post-buttons">
			{{link_to_route('cart.empty', trans('cart.action.empty'), null, ['class'=>'button secondary'])}}
			{{link_to_route('checkout', trans('cart.action.checkout'), null, ['class'=>'button primary'])}}
		</p>
		<table class="cart">
			<thead>
				<tr>
					<th width="90%" colspan="2">{{trans('cart.product')}}</th>
					<th width="10%" align="right">{{trans('cart.price')}}</th>
				</tr>
			</thead>
			<tbody>
			@foreach( Cart::content() as $item )
				@include('cart.row', ['item'=>$item])
			@endforeach
			<tr>
				<td align="right" colspan="2"><strong>{{trans('cart.total')}}</strong></td>
				<td align="right">{{ format_currency(Cart::total()) }}</td>
			</tr>
			</tbody>
		</table>
		<p class="post-buttons">
			{{link_to_route('cart.empty', trans('cart.action.empty'), null, ['class'=>'button secondary'])}}
			{{link_to_route('checkout', trans('cart.action.checkout'), null, ['class'=>'button primary'])}}
		</p>
	@else
		@include('global.notice', ['message'=>trans('cart.error.empty'), 'class'=>'error'])
	@endif
@stop