@extends('layouts.sidebar')

@section('title', trans('account.password.title'))

@section('sidebar')
    @include('account.menu')
@endsection

@section('content')
	<h2>Change Password</h2>

	{{Form::open()}}

		{{ Form::label('old_password', 'Old Password') }}
        {{ Form::password('old_password') }}
        {{ errors_for('old_password', $errors) }}

        {{ Form::label('new_password', 'New Password') }}
        {{ Form::password('new_password') }}
        {{ errors_for('new_password', $errors) }}

        {{ Form::label('new_password_confirmation', 'Confirm Password') }}
        {{ Form::password('new_password_confirmation') }}
        {{ errors_for('new_password_confirmation', $errors) }}

        {{ Form::submit('Change Password', ['class'=>'button']) }}

	{{Form::close()}}
@stop