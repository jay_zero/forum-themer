@extends('layouts.sidebar')

@section('title', trans('account.downloads.title'))

@section('sidebar')
    @include('account.menu')
@endsection

@section('content')
	@if( $downloads->count() )
		<ul class="downloads">
		@foreach( $downloads as $download )
			<li class="download">
				<div class="download--image">
					<img src="http://placehold.it/100x100" alt="{{$download->project->title}}" />
				</div>

				<div class="download--information">
					<h3>{{link_to_route( 'project.single', $download->project->title, ['premium', $download->project->software->slug, $download->project->slug])}}</h3>
				</div>

				<div class="download--options">
					{{icon_button_route( 'download.premium', 'Download', [$download->project->id, $download->token], 'fa-download', 'primary')}}
				</div>
			</li>
		@endforeach
		</ul>

		<div class="pagination">
          <ul>
            {{ with( new Themer\Presenters\PaginatePresenter( $downloads ) )->render() }}
          </ul>
        </div>
	@else
		@include('global.notice', ['class'=>'error', 'message'=>'You\'ve not made any purchases at Forum Themer just yet!' ])
	@endif
@stop