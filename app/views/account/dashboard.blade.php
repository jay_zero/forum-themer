@extends('layouts.sidebar')

@section('title', trans('account.dashboard.title'))

@section('sidebar')
    @include('account.menu')
@endsection

@section('content')
	<h2>Latest Purchases</h2>
	<p>Show latest purchases</p>
@stop