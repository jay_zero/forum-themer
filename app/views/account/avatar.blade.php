@extends('layouts.sidebar')

@section('title', trans('account.password.title'))

@section('sidebar')
    @include('account.menu')
@endsection

@section('content')
	<h2>Change Avatar</h2>

	{{Form::open(['files'=>true])}}

		{{ Form::label('avatar', 'New Avatar') }}
        {{ Form::file('avatar') }}
        {{ errors_for('avatar', $errors) }}

        {{ Form::label('remove_avatar', 'Remove Avatar?') }}
        {{ Form::checkbox('remove_avatar') }}
        {{ errors_for('remove_avatar', $errors) }}

        {{ Form::submit('Change Avatar', ['class'=>'button']) }}

	{{Form::close()}}
@stop