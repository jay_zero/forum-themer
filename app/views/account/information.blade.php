@extends('layouts.sidebar')

@section('title', trans('account.information.title'))

@section('sidebar')
    @include('account.menu')
@endsection

@section('content')
	<h2>Change Password</h2>

	{{Form::model($user)}}

		{{ Form::label('first_name', 'First Name') }}
        {{ Form::text('first_name', null, ['placeholder' => 'First Name']) }}
        {{ errors_for('first_name', $errors) }}

        {{ Form::label('last_name', 'Last Name') }}
        {{ Form::text('last_name', null, ['placeholder' => 'Last Name']) }}
        {{ errors_for('last_name', $errors) }}

        {{ Form::label('email', 'Email') }}
        {{ Form::text('email', null, ['placeholder' => 'Email']) }}
        {{ errors_for('email', $errors) }}

        <label>{{ Form::checkbox('email_updates') }} Receive periodic updates from Forum Themer? </label>
        <p>Subscribe me to the Forum Themer periodic newsletter.</p>

        {{ Form::label('password', 'Password') }}
        {{ Form::password('password') }}
        {{ errors_for('password', $errors) }}

        {{ Form::submit('Update Information', ['class'=>'button']) }}

	{{Form::close()}}
@stop