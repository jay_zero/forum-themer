@extends('layouts.full')

@section('title', $user->username)

@section('content')

	<div class="user--info">
		<h2>{{$user->username}}</h2>
		<div class="user--info_avatar">
			{{$user->present()->avatar_image}}
		</div>
		<div class="user--info_info">
			<ul>
				<li>{{trans('profile.joined')}} <span>{{$user->present()->join_date}}</span></li>
				<li>{{trans('profile.posts')}} <span>{{$user->post_count}}</span></li>
				<li>{{trans('profile.reputation')}} <span>{{$likes}}</span></li>
			</ul>
		</div>
	</div>

	<div class="user--posts">
		<h2>{{Lang::get('profile.activity')}}</h2>
		@if( $user->posts->count() )
			<section class="thread">
				@foreach( $user->posts as $reply )
					@include('forum.postbit', ['reply'=>$reply])
			    @endforeach
			</section>
		@else
			@include('forum.notice', ['message'=>trans('profile.error.no_posts')])
		@endif
	</div>
	
@stop