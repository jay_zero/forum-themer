@extends('layouts.sidebar')

@section('title', trans('account.transactions.title'))

@section('sidebar')
    @include('account.menu')
@endsection

@section('content')
	@if( $transactions->count() )
		<table>
		<tr>
				<th>Order ID</th>
				<th>Order Total</th>
				<th>Order Placed</th>
			</tr>
		@foreach( $transactions as $order )
			<tr>
				<td>{{$order->id}}</td>
				<td>{{format_currency($order->total)}}</td>
				<td>{{$order->created_at}}</td>
			</tr>
		@endforeach
		</table>
	@else
		@include('global.notice', ['class'=>'error', 'message'=>'You\'ve not made any purchases at Forum Themer just yet!' ])
	@endif
@stop