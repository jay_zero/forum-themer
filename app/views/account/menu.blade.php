<div class="sidebar--widget">
	<h4>My Account</h4>
    <ul class="links">
        <li>{{link_to_route('account.dashboard', 'Dashboard')}}</li>
        <li>{{link_to_route('account.information', 'Update Information')}}</li>
        <li>{{link_to_route('account.avatar', 'Change Avatar')}}</li>
        <li>{{link_to_route('account.password', 'Change Password')}}</li>
        <li>{{link_to_route('account.downloads', 'Downloads')}}</li>
        <li>{{link_to_route('account.transactions', 'Transaction History')}}</li>
    </ul>
</div>