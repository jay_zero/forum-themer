<div class="project">

	<a href="{{ URL::route('project.single', [$type, $project->software->slug, $project->slug])}}" class="project--image">
		<img src="{{ URL::asset('assets/images/themes/' . $project->small_image) }}" alt="{{$project->title}}" />
		<span class="project--image_overlay"></span>
		<div class="project--image_buttons">
			<span>More Details</span>
		</div>
	</a>

	<div class="project--info">
		<h4><a href="{{ URL::route('project.single', [$type, $project->software->slug, $project->slug])}}">{{ $project->title }}</a></h4>
		@if( $type == "premium" )
			<p class="price">{{ $project->present()->price_actual }}</p>
		@endif
	</div>
 </div>