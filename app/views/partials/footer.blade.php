<footer class="footer">

    <div class="container">
        <div class="footer--block">
            <h3>Content</h3>
            <ul>
                <li>{{link_to_route('projects.free', 'Free Themes')}}</li>
                <li>{{link_to_route('projects.premium', 'Premium Themes')}}</li>
                <li>{{link_to_route('forum.index', 'Support Forum')}}</li>
                @if( Sentry::getUser() )
                    <li><a href="mailto:forum-themer@helpful.io" data-helpful="forum-themer" data-helpful-name="{{Sentry::getUser()->username}}" data-helpful-email="{{Sentry::getUser()->email}}">Customer Support</a></li>
                @else
                    <li><a href="mailto:forum-themer@helpful.io" data-helpful="forum-themer">Customer Support</a></li>
                @endif
            </ul>
        </div>

        <div class="footer--block">
            <h3>My Account</h3>
            <ul>
                <li>{{link_to_route('account.dashboard', 'Dashboard')}}</li>
                <li>{{link_to_route('account.downloads', 'My Downloads')}}</li>
                <li>{{link_to_route('account.transactions', 'Order History')}}</li>
            </ul>
        </div>

        <div class="footer--block">
            <h3>Extra</h3>
            <ul>
                <li><a href="javascript:void(0)">Terms &amp; Conditions</a></li>
                <li><a href="javascript:void(0)">Privacy Policy</a></li>
                <li><a href="javascript:void(0)">Returns &amp; Refunds</a></li>
            </ul>
        </div>

        <div class="footer--block">
            <h3>About Us</h3>
            <p>Forum Themer aims to provide you with excellent free and premium themes to make your forum stand out above everyone else.</p>
            <p>Forum Themer aims to provide you with excellent free and premium themes to make your forum stand out above everyone else.</p>
        </div>

    </div>

    <div class="footer--disclaimer">
        <div class="container">
            <p>&copy; Copyright <a href="{{URL::route('home')}}">Forum Themer</a> {{date('Y')}}</p>
            <p><a href="http://www.jamiewatson.me">Web Design Staffordshire</a></p>
        </div>
    </div>

</footer>
