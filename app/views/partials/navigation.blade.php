<nav class="primary-menu">
    <ul>
        <li>{{link_to('/', 'Home')}}</li>
        <li>{{link_to_route('projects.free', 'Free Themes')}}</li>
        <li>{{link_to_route('projects.premium', 'Premium Themes')}}</li>
        <li>{{link_to_route('forum.index', 'Support Forum')}}</li>
    </ul>
</nav>