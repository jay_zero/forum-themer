@if( !Sentry::check() )
	<p>Welcome <strong>Guest</strong>, {{ link_to_route('auth.login', 'Login' ) }} or {{ link_to_route('auth.register', 'Sign Up' ) }}</a></p>
@else
	<p>Welcome back <strong>{{Sentry::getUser()->username}}</strong>, {{ link_to_route('auth.logout', 'Logout' ) }}</a></p>
@endif

<nav class="user-menu">
	<ul>
		<li>{{ link_to_route('account.dashboard', 'My Account' ) }}</li>
		@if( Sentry::getUser() && Sentry::getUser()->hasAccess('admin') )
			<li>{{ link_to_route('admin.dashboard', 'Admin CP' ) }}</li>
		@endif
		<li>{{ link_to_route('checkout', 'Checkout' ) }}</li>
		<!-- <li><a href="javascript:void(0)"><img src="{{URL::asset('assets/images/gb.png')}}" /></a></li> -->
		<!-- <li><a href="javascript:void(0)"><img src="{{URL::asset('assets/images/es.png')}}" /></a></li> -->
	</ul>
</nav>