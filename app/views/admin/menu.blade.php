<div class="sidebar--widget">
	<h4>Navigation</h4>
	<ul class="links">
        <li>{{link_to_route('admin.dashboard', 'Dashboard')}}</li>
        <li>{{link_to_route('account.information', 'Themes')}}</li>
        <li>{{link_to_route('account.password', 'Transactions')}}</li>
        <li>{{link_to_route('account.downloads', 'Users')}}</li>
        <li>{{link_to_route('account.transactions', 'Mass Email')}}</li>
    </ul>
</div>