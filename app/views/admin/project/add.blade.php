@extends('layouts.full')


@section('content')

	{{Form::open(['files'=>true])}}

		{{ Form::label('title', 'Title') }}
		{{ Form::text('title', null, ['placeholder' => 'Title']) }}
		{{ errors_for('title', $errors) }}

		{{ Form::label('slug', 'Slug') }}
		{{ Form::text('slug', null, ['placeholder' => 'Slug']) }}
		{{ errors_for('slug', $errors) }}

		{{ Form::label('price', 'Price') }}
		{{ Form::text('price', null, ['placeholder' => 'Price']) }}
		{{ errors_for('price', $errors) }}

		{{ Form::label('short_desc', 'Short Description') }}
		{{ Form::textarea('short_desc', null) }}
		{{ errors_for('short_desc', $errors) }}

		{{ Form::label('body', 'Main Description') }}
		{{ Form::textarea('body', null) }}
		{{ errors_for('body', $errors) }}

		{{ Form::label('images[]', 'Images') }}
		{{ Form::file('images[]', ['multiple']) }}
		{{ errors_for('images', $errors) }}

		{{ Form::label('file', 'File') }}
		{{ Form::file('file') }}
		{{ errors_for('file', $errors) }}

		{{ Form::submit('Add Project') }}

	{{ Form::close() }}

@stop