@extends('layouts.sidebar')


@section('sidebar')
   @include('admin.menu')
@endsection

@section('content')
	<h2>Admin Dashboard</h2>
	{{ $stats->total_orders }}<br/>
	{{ $stats->average_spend }}<br />
	{{ $stats->items_per_basket }}<br />
	{{ $stats->users }} Members<br />

	<h2>Last 10 Orders</h2>
	@foreach( $orders as $order )
		<div>{{format_currency($order->total)}}</div>
	@endforeach
@stop