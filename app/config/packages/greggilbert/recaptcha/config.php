<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6LeGx_4SAAAAAJuk5s7ssWIksn9PKCxKlmcE7yVY',
	'private_key'	=> '6LeGx_4SAAAAANSYcn4u4Lpa303PhIVVvolxBrEn',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> '',
	
);