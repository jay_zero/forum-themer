<?php

return array(
	'api_key' 			=> getenv('STRIPE_API_KEY'),
	'publishable_key' 	=> getenv('STRIPE_PUBLIC_KEY')
);